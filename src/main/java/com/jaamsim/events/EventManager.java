/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2002-2014 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.events;

import com.jaamsim.basicsim.Entity;
import com.jaamsim.basicsim.EntityManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The EventManager is responsible for scheduling future events, controlling
 * conditional event evaluation, and advancing the simulation time. Events are
 * scheduled in based on:
 * <ul>
 * <li>1 - The execution time scheduled for the event
 * <li>2 - The priority of the event (if scheduled to occur at the same time)
 * <li>3 - If both 1) and 2) are equal, the user specified FIFO or LIFO order
 * </ul>
 * <p/>
 * The event time is scheduled using a backing long value. Double valued time is
 * taken in by the scheduleWait function and scaled to the nearest long value
 * using the simTimeFactor.
 * <p/>
 * Most EventManager functionality is available through static methods that rely
 * on being in a running model context which will access the eventmanager that is
 * currently running, think of it like a thread-local variable for all model threads.
 */
public final class EventManager {

    private static final Logger LOG = LogManager.getFormatterLogger(EventManager.class.getSimpleName());

    public final String name;

    private final Object lockObject; // Object used as global lock for synchronization

    private volatile Process currentProcess;

    private final EventTree eventTree;
    private final ArrayList<ConditionalEvent> conditionalEvents;
    private volatile boolean executeEvents;
    private boolean processRunning;
    private long currentTick; // Master simulation time (long)
    private long nextTick; // The next tick to execute events at
    private long targetTick; // the largest time we will execute events for (run to time)

    private double ticksPerSecond; // The number of discrete ticks per simulated second
    private double secsPerTick;    // The length of time in seconds each tick represents

    // Real time execution state
    private long realTimeTick;    // the simulation tick corresponding to the wall-clock millis value
    private long realTimeMillis;  // the wall-clock time in millis

    private volatile boolean executeRealTime;  // TRUE if the simulation is to be executed in Real Time mode
    private volatile boolean rebaseRealTime;   // TRUE if the time keeping for Real Time model needs re-basing
    private volatile int realTimeFactor;       // target ratio of elapsed simulation time to elapsed wall clock time

    private EventTimeListener timelistener;
    private EventErrorListener errListener;
    private EventTraceListener trcListener;
    private Event freeEvents = null;

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Allocates a new EventManager with the given parent and name
     *
     * @param name   the name this EventManager should use
     */
    public EventManager(String name) {
        LOG.debug("Adding event manager");
        // Basic initialization
        this.name = name;
        this.entityManager = entityManager;

        lockObject = new Object();

        // Initialize and event lists and timekeeping variables
        currentTick = 0;
        nextTick = 0;

        setTickLength(1e-6d);

        eventTree = new EventTree();
        conditionalEvents = new ArrayList<>();

        executeEvents = false;
        processRunning = false;
        executeRealTime = false;
        realTimeFactor = 1;
        rebaseRealTime = true;
        setTimeListener(null);
        setErrorListener(null);
    }

    /**
     * Pause the execution of the current Process and schedule it to wake up at a future
     * time in the controlling EventManager,
     *
     * @param secs          the number of seconds in the future to wake at
     * @param priority      the priority of the scheduled wakeup event
     * @param fifo          break ties with previously scheduled events using FIFO/LIFO ordering
     * @param handle        an optional handle to hold onto the scheduled event
     * @throws ProcessError if called outside of a Process context
     */
    public final void waitSeconds(double secs, int priority, boolean fifo, EventHandle handle) {
        long ticks = secondsToNearestTick(secs);
        waitTicks(ticks, priority, fifo, handle);
    }

    public final long secondsToTicks(double secs) {
        return secondsToNearestTick(secs);
    }

    public final double calcSimTime(double secs) {
        long ticks = secondsToNearestTick(secs) + currentTick;
        if (ticks < 0)
            ticks = Long.MAX_VALUE;

        return ticksToSeconds(ticks);
    }

    public final long calcSimTicks(double secs) {
        long ticks = secondsToNearestTick(secs) + currentTick;
        if (ticks < 0)
            ticks = Long.MAX_VALUE;

        return ticks;
    }

    /**
     * Schedule a future event in the controlling EventManager for the current Process.
     *
     * @param secs          the number of seconds in the future to schedule this event
     * @param eventPriority the priority of the scheduled event
     * @param fifo          break ties with previously scheduled events using FIFO/LIFO ordering
     * @param t             the process target to run when the event is executed
     * @param handle        an optional handle to hold onto the scheduled event
     * @throws ProcessError if called outside of a Process context
     */
    public final void scheduleSeconds(double secs, int eventPriority, boolean fifo, ProcessTarget t, EventHandle handle) {
        long ticks = secondsToNearestTick(secs);
        scheduleTicks(ticks, eventPriority, fifo, t, handle);
    }

    /**
     * Returns whether or not we are currently running in a Process context
     * that has a controlling EventManager.
     *
     * @return true if we are in a Process context, false otherwise
     */
    public final boolean hasCurrent() {
// TODO dismiss        return (Thread.currentThread() instanceof Process);
        // TODO there must be a better solution
        return (Thread.currentThread().getName().contains("processThread-"));
    }

    /**
     * Returns the controlling EventManager for the current Process.
     *
     * @throws ProcessError if called outside of a Process context
     */
//    public final EventManager getCurrentEventManager() {
//        return this;
//    }

    /**
     * Returns the current simulation tick for the current Process.
     *
     * @throws ProcessError if called outside of a Process context
     */
    public final long simTicks() {
        getCurrentProcess();
        return currentTick;
    }

    /**
     * Returns the current simulation time in seconds for the current Process.
     *
     * @throws ProcessError if called outside of a Process context
     */
    public final double simSeconds() {
        getCurrentProcess();
        return _simSeconds();
    }

    public final void setTimeListener(EventTimeListener l) {
        synchronized (lockObject) {
            if (l != null)
                timelistener = l;
            else
                timelistener = new NoopListener();

            timelistener.tickUpdate(currentTick);
        }
    }

    public final void setErrorListener(EventErrorListener l) {
        synchronized (lockObject) {
            if (l != null)
                errListener = l;
            else
                errListener = new NoopListener();
        }
    }

    public final void setTraceListener(EventTraceListener l) {
        synchronized (lockObject) {
            trcListener = l;
        }
    }

    public void clear() {
        synchronized (lockObject) {
            currentTick = 0;
            nextTick = 0;
            targetTick = Long.MAX_VALUE;
            timelistener.tickUpdate(currentTick);
            rebaseRealTime = true;

            eventTree.runOnAllNodes(new KillAllEvents());
            eventTree.reset();
            clearFreeList();

            for (int i = 0; i < conditionalEvents.size(); i++) {
                conditionalEvents.get(i).target.kill();
                if (conditionalEvents.get(i).handle != null) {
                    conditionalEvents.get(i).handle.event = null;
                }
            }
            conditionalEvents.clear();
        }
    }

    private boolean executeTarget(Process currentProcess, ProcessTarget targetProcess) {
        try {
            // If the event has a captured process, pass control to it
            Process capturedProcess = targetProcess.getProcess();
            if (capturedProcess != null) {
                capturedProcess.setNextProcess(currentProcess);
                capturedProcess.wake();
                threadWait(currentProcess);
                return true;
            }

            // Execute the method
            targetProcess.process();

            // Notify the event manager that the process has been completed
            if (trcListener != null) trcListener.traceProcessEnd(this, currentTick);

            if (currentProcess.hasNext()) {
                currentProcess.wakeNextProcess();
                return false;
            } else {
                return true;
            }
        } catch (Throwable e) {
            // This is how kill() is implemented for sleeping processes.
            LOG.error("While executing target: ", e);
            if (e instanceof ThreadKilledException)
                return false;

            // Tear down any threads waiting for this to finish
            Process next = currentProcess.forceKillNext();
            while (next != null) {
                next = next.forceKillNext();
            }
            executeEvents = false;
            processRunning = false;
            errListener.handleError(this, e, currentTick);
            return false;
        }
    }

    /**
     * Main event execution method the eventManager, this is the only entrypoint
     * for Process objects taken out of the pool.
     */
    final void execute(Process currentProcess, ProcessTarget targetProcess) {
        synchronized (lockObject) {
            // TODO think to refactor to a better association strategy
            this.currentProcess = currentProcess;
            // This occurs in the startProcess or interrupt case where we start
            // a process with a target already assigned
            if (targetProcess != null) {
                executeTarget(currentProcess, targetProcess);
                return;
            }

            if (processRunning)
                return;

            processRunning = true;
            timelistener.timeRunning(true);

            // Loop continuously
            while (true) {
                LOG.debug("Events tree: " + eventTree);

                EventNode nextNode = eventTree.getNextNode();
                if (nextNode == null || currentTick >= targetTick) {
                    executeEvents = false;
                }

                if (!executeEvents) {
                    processRunning = false;
                    timelistener.timeRunning(false);
                    return;
                }

                // If the next event is at the current tick, execute it
                if (nextNode.schedTick == currentTick) {
                    // Remove the event from the future events
                    Event nextEvent = nextNode.head;
                    ProcessTarget nextTarget = nextEvent.target;
                    if (trcListener != null)
                        trcListener.traceEvent(this, currentTick, nextNode.schedTick, nextNode.priority, nextTarget);

                    removeEvent(nextEvent);

                    // the return from execute target informs whether or not this
                    // thread should grab an new Event, or return to the pool
                    if (executeTarget(currentProcess, nextTarget))
                        continue;
                    else
                        return;
                }

                // If the next event would require us to advance the time, check the
                // conditonal events
                if (eventTree.getNextNode().schedTick > nextTick) {
                    if (conditionalEvents.size() > 0) {
                        evaluateConditions(currentProcess);
                        if (!executeEvents) continue;
                    }

                    // If a conditional event was satisfied, we will have a new event at the
                    // beginning of the eventStack for the current tick, go back to the
                    // beginning, otherwise fall through to the time-advance
                    nextTick = eventTree.getNextNode().schedTick;
                    if (nextTick == currentTick)
                        continue;
                }

                // Advance to the next event time
                if (executeRealTime) {
                    // Loop until the next event time is reached
                    long realTick = this.calcRealTimeTick();
                    if (realTick < nextTick && realTick < targetTick) {
                        // Update the displayed simulation time
                        currentTick = realTick;
                        timelistener.tickUpdate(currentTick);
                        //Halt the thread for 20ms and then reevaluate the loop
                        try {
                            lockObject.wait(20);
                        } catch (InterruptedException e) {
                        }
                        continue;
                    }
                }

                // advance time
                if (targetTick < nextTick)
                    currentTick = targetTick;
                else
                    currentTick = nextTick;

                timelistener.tickUpdate(currentTick);
            }
        }
    }

    private void evaluateConditions(Process cur) {
        cur.begConditionalWait();
        try {
            for (int i = 0; i < conditionalEvents.size(); ) {
                ConditionalEvent c = conditionalEvents.get(i);
                if (c.c.evaluate()) {
                    conditionalEvents.remove(i);
                    EventNode node = getEventNode(currentTick, 0);
                    Event evt = getEvent();
                    evt.node = node;
                    evt.target = c.target;
                    evt.handle = c.handle;
                    if (evt.handle != null) {
                        // no need to check the handle.isScheduled as we just unscheduled it above
                        // and we immediately switch it to this event
                        evt.handle.event = evt;
                    }
                    if (trcListener != null) trcListener.traceWaitUntilEnded(this, currentTick, c.target);
                    node.addEvent(evt, true);
                    continue;
                }
                i++;
            }
        } catch (Throwable e) {
            executeEvents = false;
            processRunning = false;
            errListener.handleError(this, e, currentTick);
        }

        cur.endConditionalWait();
    }

    /**
     * Return the simulation time corresponding the given wall clock time
     *
     * @return simulation time in seconds
     */
    private long calcRealTimeTick() {
        long curMS = System.currentTimeMillis();
        if (rebaseRealTime) {
            realTimeTick = currentTick;
            realTimeMillis = curMS;
            rebaseRealTime = false;
        }

        double simElapsedsec = ((curMS - realTimeMillis) * realTimeFactor) / 1000.0d;
        long simElapsedTicks = secondsToNearestTick(simElapsedsec);
        return realTimeTick + simElapsedTicks;
    }

    /**
     * // Pause the current active thread and restart the next thread on the
     * // active thread list. For this case, a future event or conditional event
     * // has been created for the current thread.  Called by
     * // eventManager.scheduleWait() and related methods, and by
     * // eventManager.waitUntil().
     * // restorePreviousActiveThread()
     * Must hold the lockObject when calling this method.
     */
    private void captureProcess(Process cur) {
        // if we don't wake a new process, take one from the pool
        Process next = cur.preCapture();
        if (next == null) {
            processRunning = false;
            Process.processEvents(this);
        } else {
            next.wake();
        }

        threadWait(cur);
        cur.postCapture();
    }

    /**
     * Calculate the time for an event taking into account numeric overflow.
     * Must hold the lockObject when calling this method
     */
    private long calculateEventTime(long waitLength) {
        // Test for negative duration schedule wait length
        if (waitLength < 0)
            throw new ProcessError("Negative duration wait is invalid, waitLength = " + waitLength);

        // Check for numeric overflow of internal time
        long nextEventTime = currentTick + waitLength;
        if (nextEventTime < 0)
            nextEventTime = Long.MAX_VALUE;

        return nextEventTime;
    }

    /**
     * Schedules a future event to occur with a given priority.  Lower priority
     * events will be executed preferentially over higher priority.  This is
     * by lower priority events being placed higher on the event stack.
     *
     * @param ticks    the number of discrete ticks from now to schedule the event.
     * @param priority the priority of the scheduled event: 1 is the highest priority (default is priority 5)
     */
    public void waitTicks(long ticks, int priority, boolean fifo, EventHandle handle) {
        synchronized (lockObject) {
            Process current = getCurrentProcess();
            current.checkConditionalWait();
            long nextEventTime = calculateEventTime(ticks);
            WaitTarget t = new WaitTarget(current);
            EventNode node = getEventNode(nextEventTime, priority);
            Event evt = getEvent();
            evt.node = node;
            evt.target = t;
            evt.handle = handle;
            if (handle != null) {
                if (handle.isScheduled())
                    throw new ProcessError("Tried to schedule using an EventHandle already in use");
                handle.event = evt;
            }

            if (trcListener != null) trcListener.traceWait(this, currentTick, nextEventTime, priority, t);
            node.addEvent(evt, fifo);
            captureProcess(current);
        }
    }

    /**
     * Find an eventNode in the list, if a node is not found, create one and
     * insert it.
     */
    private EventNode getEventNode(long tick, int prio) {
        return eventTree.createOrFindNode(tick, prio);
    }

    private Event getEvent() {
        if (freeEvents != null) {
            Event evt = freeEvents;
            freeEvents = evt.next;
            return evt;
        }

        return new Event();
    }

    private void clearFreeList() {
        freeEvents = null;
    }

    /**
     * Used to achieve conditional waits in the simulation.  Adds the calling
     * thread to the conditional stack, then wakes the next waiting thread on
     * the thread stack.
     */
    public void waitUntil(Conditional cond, EventHandle handle) {
        Process current = getCurrentProcess();
        synchronized (lockObject) {
            current.checkConditionalWait();
            WaitTarget t = new WaitTarget(current);
            ConditionalEvent evt = new ConditionalEvent(cond, t, handle);
            if (handle != null) {
                if (handle.isScheduled())
                    throw new ProcessError("Tried to waitUntil using a handle already in use");
                handle.event = evt;
            }
            conditionalEvents.add(evt);
            if (trcListener != null) trcListener.traceWaitUntil(this, currentTick);
            captureProcess(current);
        }
    }

    public void scheduleUntil(ProcessTarget t, Conditional cond, EventHandle handle) {
        Process current = getCurrentProcess();
        synchronized (lockObject) {
            current.checkConditionalWait();
            ConditionalEvent evt = new ConditionalEvent(cond, t, handle);
            if (handle != null) {
                if (handle.isScheduled())
                    throw new ProcessError("Tried to scheduleUntil using a handle already in use");
                handle.event = evt;
            }
            conditionalEvents.add(evt);
            if (trcListener != null) trcListener.traceWaitUntil(this, currentTick);
        }
    }

    public void startProcess(ProcessTarget processTarget) {
        Process current = getCurrentProcess();
        Process newProcess = Process.allocate(this, getCurrentProcess(), processTarget);
        // Notify the eventManager that a new process has been started
        synchronized (lockObject) {
            current.checkConditionalWait();
            if (trcListener != null) trcListener.traceProcessStart(this, processTarget, currentTick);
            // Transfer control to the new process
            newProcess.wake();
            threadWait(current);
        }
    }

    /**
     * Remove an event from the eventList, must hold the lockObject.
     *
     * @param evt
     * @return
     */
    private void removeEvent(Event evt) {
        EventNode node = evt.node;
        node.removeEvent(evt);
        if (node.head == null) {
            if (!eventTree.removeNode(node.schedTick, node.priority))
                throw new ProcessError("Tried to remove an eventnode that could not be found");
        }

        // Clear the event to reuse it
        evt.node = null;
        evt.target = null;
        if (evt.handle != null) {
            evt.handle.event = null;
            evt.handle = null;
        }

        evt.next = freeEvents;
        freeEvents = evt;
    }

    private ProcessTarget remove(EventHandle handle) {
        BaseEvent base = handle.event;
        ProcessTarget processTarget = base.target;
        handle.event = null;
        base.handle = null;
        if (base instanceof Event) {
            removeEvent((Event) base);
        } else {
            conditionalEvents.remove(base);
        }
        return processTarget;
    }

    /**
     * Removes an event from the pending list without executing it.
     */
    public void killEvent(EventHandle handle) {
        Process current = getCurrentProcess();
        synchronized (lockObject) {
            current.checkConditionalWait();

            // no handle given, or Handle was not scheduled, nothing to do
            if (handle == null || handle.event == null)
                return;

            if (trcListener != null) trcKill(handle.event);
            ProcessTarget t = remove(handle);

            t.kill();
        }
    }

    private void trcKill(BaseEvent event) {
        if (event instanceof Event) {
            EventNode node = ((Event) event).node;
            trcListener.traceKill(this, currentTick, node.schedTick, node.priority, event.target);
        } else {
            trcListener.traceKill(this, currentTick, -1, -1, event.target);
        }
    }

    /**
     * Removes an event from the pending list and executes it.
     */
    public final void interruptEvent(EventHandle handle) {
        Process currentProcess = getCurrentProcess();
        synchronized (lockObject) {
            currentProcess.checkConditionalWait();

            // no handle given, or Handle was not scheduled, nothing to do
            if (handle == null || handle.event == null)
                return;

            if (trcListener != null) trcInterrupt(handle.event);
            ProcessTarget processTarget = remove(handle);

            Process process = processTarget.getProcess();
            if (process == null)
                process = Process.allocate(this, currentProcess, processTarget);
            process.setNextProcess(currentProcess);
            process.wake();
            threadWait(currentProcess);
        }
    }

    private void trcInterrupt(BaseEvent event) {
        if (event instanceof Event) {
            EventNode node = ((Event) event).node;
            trcListener.traceInterrupt(this, currentTick, node.schedTick, node.priority, event.target);
        } else {
            trcListener.traceInterrupt(this, currentTick, -1, -1, event.target);
        }
    }

    public void setExecuteRealTime(boolean useRealTime, int factor) {
        executeRealTime = useRealTime;
        realTimeFactor = factor;
        if (useRealTime)
            rebaseRealTime = true;
    }

    /**
     * Locks the calling thread in an inactive state to the global lock.
     * When a new thread is created, and the current thread has been pushed
     * onto the inactive thread stack it must be put to sleep to preserve
     * program ordering.
     * <p/>
     * The function takes no parameters, it puts the calling thread to sleep.
     * This method is NOT static as it requires the use of wait() which cannot
     * be called from a static context
     * <p/>
     * There is a synchronized block of code that will acquire the global lock
     * and then wait() the current thread.
     */
    private void threadWait(Process currentProcess) {
        // Ensure that the thread owns the global thread lock
        try {
            /*
			 * Halt the thread and only wake up by being interrupted.
			 *
			 * The infinite loop is _absolutely_ necessary to prevent
			 * spurious wakeups from waking us early....which causes the
			 * model to get into an inconsistent state causing crashes.
			 */
            while (true) {
                lockObject.wait();
            }
        } catch (InterruptedException e) {
            // Catch the exception when the thread is interrupted, exit condition
        }

        if (currentProcess.shouldDie())
            throw new ThreadKilledException("Thread killed");

        this.currentProcess = currentProcess;
    }

    public void scheduleProcessExternal(long waitLength, int eventPriority, boolean fifo, ProcessTarget processTarget, EventHandle handle) {
        synchronized (lockObject) {
            long scheduledTick = calculateEventTime(waitLength);
            EventNode node = getEventNode(scheduledTick, eventPriority);
            Event event = getEvent();
            event.node = node;
            event.target = processTarget;
            event.handle = handle;
            if (handle != null) {
                if (handle.isScheduled())
                    throw new ProcessError("Tried to schedule using an EventHandle already in use");
                handle.event = event;
            }
            if (trcListener != null) trcListener.traceSchedProcess(this, currentTick, scheduledTick, eventPriority, processTarget);
            node.addEvent(event, fifo);
        }
    }

    public void scheduleTicks(long waitLength, int eventPriority, boolean fifo, ProcessTarget t, EventHandle handle) {
        Process current = getCurrentProcess();
        current.checkConditionalWait();
        long schedTick = calculateEventTime(waitLength);
        EventNode node = getEventNode(schedTick, eventPriority);
        Event evt = getEvent();
        evt.node = node;
        evt.target = t;
        evt.handle = handle;
        if (handle != null) {
            if (handle.isScheduled())
                throw new ProcessError("Tried to schedule using an EventHandle already in use");
            handle.event = evt;
        }
        if (trcListener != null) trcListener.traceSchedProcess(this, currentTick, schedTick, eventPriority, t);
        node.addEvent(evt, fifo);
    }

    /**
     * Sets the value that is tested in the doProcess loop to determine if the
     * next event should be executed.  If set to false, the eventManager will
     * execute a threadWait() and wait until an interrupt is generated.  It is
     * guaranteed in this state that there is an empty thread stack and the
     * thread referenced in activeThread is the eventManager thread.
     */
    public void pause() {
        executeEvents = false;
    }

    public void stop() {
        reportListeners.clear();
//        if(currentProcess != null) {
//            currentProcess.stop();
//        }
        currentProcess.shutdown();
        currentProcess = null;
    }

    /**
     * Sets the value that is tested in the doProcess loop to determine if the
     * next event should be executed.  Generates an interrupt of activeThread
     * in case the eventManager thread has already been paused and needs to
     * resume the event execution loop.  This prevents the model being resumed
     * from an inconsistent state.
     */
    public void resume(long targetTicks) {
        synchronized (lockObject) {
            targetTick = targetTicks;
            rebaseRealTime = true;
            if (executeEvents)
                return;

            executeEvents = true;
            Process.processEvents(this);
        }
    }

    @Override
    public String toString() {
        return name + "[" + super.toString() + "]";
    }

    // TODO was private
    public double _simSeconds() {
        return currentTick * secsPerTick;
    }

    public final void setTickLength(double tickLength) {
        secsPerTick = tickLength;
        ticksPerSecond = Math.round(1e9d / secsPerTick) / 1e9d;
    }

    /**
     * Convert the number of seconds rounded to the nearest tick.
     */
    public final long secondsToNearestTick(double seconds) {
        return Math.round(seconds * ticksPerSecond);
    }

    /**
     * Convert the number of ticks into a value in seconds.
     */
    public final double ticksToSeconds(long ticks) {
        return ticks * secsPerTick;
    }

    public boolean isProcessRunning() {
        return processRunning;
    }

    private static class KillAllEvents implements EventNode.Runner {
        @Override
        public void runOnNode(EventNode node) {
            Event each = node.head;
            while (each != null) {
                if (each.handle != null) {
                    each.handle.event = null;
                    each.handle = null;
                }

                each.target.kill();
                each = each.next;
            }
        }
    }

    public final Process getCurrentProcess() {
        try {
            LOG.debug("Returning process from thread: " + Thread.currentThread());
            if(currentProcess == null || Thread.currentThread() != currentProcess.getCurrentThread()) {
                throw new ClassCastException();
            }
// TODO dismiss           return (Process) Thread.currentThread();
            return currentProcess;
        } catch (ClassCastException e) {
            throw new ProcessError("Non-process thread called getCurrentProcess()");
        }
    }

    private final List<ReportListener> reportListeners = new ArrayList<>();

    public final void addReportListener(ReportListener listener) {
        if (listener != null) {
            reportListeners.add(listener);
        }
    }

    public void printReport(double simTime) {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        LOG.info("Generating report");

        // Identify the classes that were used in the model
        ArrayList<Class<? extends Entity>> newClasses = new ArrayList<>();
        for (Entity ent : entityManager.getAll()) {
            if (ent.testFlag(Entity.FLAG_GENERATED))
                continue;
            if (!newClasses.contains(ent.getClass()))
                newClasses.add(ent.getClass());
        }

        // Loop through the classes and identify the instances
        for (Class<? extends Entity> newClass : newClasses) {
            for (Entity ent : entityManager.getAll()) {
                if (ent.testFlag(Entity.FLAG_GENERATED))
                    continue;
                if (ent.getClass() != newClass)
                    continue;
                try {
                    ent.printReport(outputStream, simTime);
                } catch (IOException e) {
                    LOG.error("Error in generating the report: " + e.getMessage());
                    return;
                }
            }
        }

        for(ReportListener listener : reportListeners) {
            listener.save(outputStream.toByteArray());
        }
    }
}
