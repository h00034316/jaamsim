/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2002-2014 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.events;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.*;

/**
 * Process is a subclass of Thread that can be managed by the discrete event
 * simulation.
 * <p/>
 * This is the basis for all functionality required by startProcess and the
 * discrete event model. Each process creates its own thread to run in. These
 * threads are managed by the eventManager and when a Process has completed
 * running is pooled for reuse.
 * <p/>
 * LOCKING: All state in the Process must be updated from a synchronized block
 * using the Process itself as the lock object. Care must be taken to never take
 * the eventManager's lock while holding the Process's lock as this can cause a
 * deadlock with other threads trying to wake you from the threadPool.
 */
final class Process implements Runnable {
    private static final Logger LOG = LogManager.getFormatterLogger(Process.class.getSimpleName());

    private String name;

    private Thread currentThread;

    // Properties required to manage the pool of available Processes
    // TODO dismiss private static final BlockingQueue<Process> pool; // storage for all available Processes

    private static final ExecutorService executorService;

    private static final int MAX_POOL_SIZE = 100; // Maximum number of Processes allowed to be pooled at a given time

    private static int numProcesses = 0; // Total of all created processes to date (used to name new Processes)

    // Initialize the storage for the pooled Processes
    static {
        // TODO dismiss pool = new LinkedBlockingQueue<>(MAX_POOL_SIZE);
        executorService = Executors.newFixedThreadPool(MAX_POOL_SIZE);
    }

    private EventManager eventManager; // The EventManager that is currently managing this Process
    private Process nextProcess; // The Process from which the present process was created
    private ProcessTarget targetProcess; // The entity whose method is to be executed
    // These are a very special references that is only safe to use from the currently
    // executing Process, they are essentially Threadlocal variables that are only valid
    // when activeFlag == true
    private EventManager evt;
    private boolean hasNext;
    private boolean dieFlag;
    private boolean activeFlag;
    private boolean conditionalWait;

    private boolean shutdown;

    private Process(String name) {
        // Construct a thread with the given name
        this.name = name;
        LOG.debug("Instantiate process: " + this);
    }

    public String getName() {
        return name;
    }

    public Thread getCurrentThread() {
        return currentThread;
    }

    public String toString() {
        return getName();
    }

    // Pull a process from the pool and have it attempt to execute events from the
    // given eventManager
    static void processEvents(EventManager eventManager) {
        LOG.debug("Processing event manager: " + eventManager);
        Process newProcess = Process.getProcess();
        newProcess.setup(eventManager, null, null);
        newProcess.wake();
    }

    // Set up a new process for the given entity, method, and arguments
    // Called from Process.start() and from EventManager.startExternalProcess()
    static Process allocate(EventManager eventManager, Process next, ProcessTarget proc) {
        Process newProcess = Process.getProcess();
        LOG.debug(String.format("Allocating next process: %s [%s]", next, Thread.currentThread()));
        newProcess.setup(eventManager, next, proc);
        return newProcess;
    }

    // Return a process from the pool or create a new one
    private static Process getProcess() {
/* TODO dismiss
        while (true) {
            synchronized (pool) {
                // If there is an available process in the pool, then use it
                if (pool.size() > 0) {
                    try {
                        Process process = pool.take();
                        LOG.debug("Taking queued process " + process.getName());
                        return process;
                    } catch (InterruptedException e) {
                        LOG.fatal("taking a new process from the pool failed by an interrupt!");
                    }
                }
                // If there are no process in the pool, then create a new one and add it to the pool
                else {
                    numProcesses++;
                    Process temp = new Process("processthread-" + numProcesses);
                    temp.start(); // Note: Thread.start() calls Process.run which adds the new process to the pool
                    LOG.debug("Launching new process " + temp.getName());
                }
            }
        }
*/

        numProcesses++;
        return new Process("processThread-" + numProcesses);
    }

    /**
     * Run method invokes the method on the target with the given arguments.
     * A process loops endlessly after it is created executing the method on the
     * target set as the entry point.  After completion, it calls endProcess and
     * will return it to a process pool if space is available, otherwise the resources
     * including the backing thread will be released.
     * <p/>
     * This method is called by Process.getProcess()
     */
    @Override
    public void run() {
        // TODO refactor for a better object reference strategy
        currentThread = Thread.currentThread();
        currentThread.setName(this.getName());
//        while (!shutdown) {
//             waitInPool();
//
//            if(shutdown) {
//                break;
//            }

            // Process has been woken up, execute the method we have been assigned
            ProcessTarget target;
            synchronized (this) {
                evt = eventManager;
                target = targetProcess;
                targetProcess = null;
                activeFlag = true;
                hasNext = (nextProcess != null);
            }

            evt.execute(this, target);

            // Ensure all state is cleared before returning to the pool
            // TODO dismiss, no more return to pool
//            evt = null;
//            hasNext = false;
//            setup(null, null, null);
//        }

        LOG.debug(String.format("Task completed for process: %s", Thread.currentThread()));

    }

    final boolean hasNext() {
        LOG.debug("Check process has next: " + hasNext);
        return hasNext;
    }

    // Useful to filter pooled threads when staring at stack traces.
    private void waitInPool() {
        // TODO dismiss
//        synchronized (pool) {
//            // Add ourselves to the pool and wait to be assigned work
//            pool.add(this);
//            // Set the present process to sleep, and release its lock
//            // (done by pool.wait();)
//            // Note: the try/while(true)/catch construct is needed to avoid
//            // spurious wake ups allowed as of Java 5.  All legitimate wake
//            // ups are done through the InterruptedException.
//            try {
//                while (true) {
//                    pool.wait();
//                }
//            } catch (InterruptedException e) {
//            }
//        }
    }

    /*
     * Setup the process state for execution.
     */
    private synchronized void setup(EventManager eventManager, Process nextProcess, ProcessTarget targetProcess) {
        LOG.debug(String.format("Setting up process: [%s] [%s] [%s] %s", eventManager, nextProcess, targetProcess, this));
        this.eventManager = eventManager;
        this.nextProcess = nextProcess;
        this.targetProcess = targetProcess;
        activeFlag = false;
        dieFlag = false;
        conditionalWait = false;
    }

    /**
     * We override this method to prevent user code from breaking the event state machine.
     * If user code explicitly interrupted a Process it would likely run event code
     * much earlier than intended.
     */
//    @Override
// TODO dismiss    public void interrupt() {
//        new Throwable("AUDIT: direct call of Process.interrupt").printStackTrace();
//    }

    /**
     * This is the wrapper to allow internal code to advance the state machine by waking
     * a Process.
     */
    final void wake() {
        LOG.debug("Waking up process: " + this.getName());

            // TODO dismiss super.interrupt();
        if(currentThread == null) {
            executorService.execute(this);
        } else {
            currentThread.interrupt();
        }
    }

    synchronized void setNextProcess(Process next) {
        LOG.debug("Setting next process: " + next);
        nextProcess = next;
    }

    /**
     * Returns true if we woke a next Process, otherwise return false.
     */
    synchronized final void wakeNextProcess() {
        LOG.debug("Waking up next process: " + nextProcess);
        nextProcess.wake();
        nextProcess = null;
        hasNext = false;
    }

    // TODO not managed by an eventManager
    synchronized void kill() {
        if (activeFlag)
            throw new ProcessError("Cannot terminate an active thread");
        dieFlag = true;
        this.wake();
    }

    /**
     * This is used to tear down a live threadstack when an error is received from
     * the model.
     */
    synchronized Process forceKillNext() {
        Process ret = nextProcess;
        nextProcess = null;
        if (ret != null) {
            ret.dieFlag = true;
            ret.wake();
        }
        return ret;
    }

    synchronized boolean shouldDie() {
        return dieFlag;
    }

    synchronized final Process preCapture() {
        LOG.debug(String.format("Pre capturing process: %s [%s]", nextProcess, Thread.currentThread()));
        activeFlag = false;
        Process ret = nextProcess;
        nextProcess = null;
        hasNext = false;
        return ret;
    }

    synchronized final void postCapture() {
        LOG.debug(String.format("Post capturing process: %s [%s]", nextProcess, Thread.currentThread()));
        activeFlag = true;
        hasNext = (nextProcess != null);
    }

    final void begConditionalWait() {
        conditionalWait = true;
    }

    final void endConditionalWait() {
        conditionalWait = false;
    }

    final void checkConditionalWait() {
        if (conditionalWait)
            throw new ProcessError("Event Control attempted from inside a Conditional callback");
    }

    public void shutdown() {
//        LOG.debug("Shutting down thread pool executor ...");
//        executorService.shutdown();
//        LOG.debug("... done!");
        currentThread = null;
        eventManager = null;
        evt = null;
    }
}
