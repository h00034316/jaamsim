/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2002-2014 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.events;

import java.util.ArrayList;

final class ProcessPool {

    private ArrayList<Process> pool; // storage for all available Processes
    private int maxPoolSize = 100; // Maximum number of Processes allowed to be pooled at a given time
    private int numProcesses = 0; // Total of all created processes to date (used to name new Processes)

    // Initialize the storage for the pooled Processes
    public ProcessPool() {
        pool = new ArrayList<>(maxPoolSize);
    }

    // Set up a new process for the given entity, method, and arguments
    // Called from Process.start() and from EventManager.startExternalProcess()
    Process allocate(EventManager eventManager, Process nextProcess, ProcessTarget targetProcess) {
        Process newProcess = getProcess();
        // TODO newProcess.setup(eventManager, nextProcess, targetProcess);
        return newProcess;
    }

    // Return a process from the pool or create a new one
    private Process getProcess() {
        while (true) {
            synchronized (pool) {
                // If there is an available process in the pool, then use it
                if (pool.size() > 0) {
                    return pool.remove(pool.size() - 1);
                }
                // If there are no process in the pool, then create a new one and add it to the pool
                else {
                    numProcesses++;
// TODO					Process process = new Process("processThread-" + numProcesses);
//					process.start(); // Note: Thread.start() calls Process.run which adds the new process to the pool
                }
            }

            // Allow the Process.run method to execute so that it can add the
            // new process to the pool
            // Note: that the lock on the pool has been dropped, so that the
            // Process.run method can grab it.
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
    }
}
