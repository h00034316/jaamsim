/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2014 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.input;

import com.jaamsim.basicsim.Entity;

import java.util.ArrayList;


public class AssignmentListInput extends ListInput<ArrayList<ExpParser.Assignment>> {

    private Entity entity;

    public AssignmentListInput(String key, String cat, ArrayList<ExpParser.Assignment> def) {
        super(key, cat, def);
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    @Override
    public void parse(KeywordIndex kw) throws InputErrorException {

        // Divide up the inputs by the inner braces
        ArrayList<KeywordIndex> subArgs = kw.getSubArgs();
        ArrayList<ExpParser.Assignment> temp = new ArrayList<>(subArgs.size());

        // Parse the inputs within each inner brace
        for (int i = 0; i < subArgs.size(); i++) {
            KeywordIndex subArg = subArgs.get(i);
            Input.assertCount(subArg, 1);
            try {
                // Parse the assignment expression
                ExpParser.Assignment assignment = ExpParser.parseAssignment(new ExpEvaluator(entityManager).getParseContext(), subArg.getArg(0));
                new ExpressionValidator(entityManager).validateAssignment(assignment, entity);

                // Save the data for this assignment
                temp.add(assignment);

            } catch (ExpressionError e) {
                throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
            }
        }

        // Save the data for each assignment
        value = temp;
    }

    @Override
    public int getListSize() {
        if (value == null)
            return 0;
        else
            return value.size();
    }

    @Override
    public String getDefaultString() {
        return "";
    }

}
