/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2009-2011 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.input;

import com.jaamsim.Thresholds.Threshold;
import com.jaamsim.Thresholds.ThresholdUser;
import com.jaamsim.basicsim.*;
import com.jaamsim.events.EventManager;
import com.jaamsim.math.Vec3d;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

public class ConfigurationManager {
    private static final Logger LOG = LogManager.getFormatterLogger(ConfigurationManager.class.getSimpleName());

    private static final String RESOURCES = "/jaamsim";
    private static final String RESOURCE_TAG = "<res>";

    public static final String RECORD_DEFINE = "DEFINE";
    public static final String RECORD_INCLUDE = "INCLUDE";
    public static final String RECORD_EDITS = "RECORDEDITS";
    public static final String RECORD_COMMENT = "\"";

    private File configurationFile;           // present configuration file
    private boolean recordEditsFound;  // TRUE if the "RecordEdits" marker is found in the configuration file
    private boolean recordEdits;       // TRUE if input changes are to be marked as edited.

    private File reportDir;
    private static URI resRoot;

    @Autowired
    private EventManager eventManager;

    public void setEventManager(EventManager eventManager) {
        this.eventManager = eventManager;
    }

    public EventManager getEventManager() {
        return eventManager;
    }

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public ConfigurationManager(EntityManager entityManager) {
        recordEditsFound = false;
        configurationFile = null;
        reportDir = null;
        this.entityManager = entityManager;
    }

    {
        try {
            // locate the resource folder, and create
            // TODO make class loader independent
            URL resource = ConfigurationManager.class.getResource(RESOURCES);
            LOG.debug("Resource path: " + resource);
            // resource = ClassLoader.getSystemResource("." + RESOURCES);
            // LOG.info("Resource from ClassLoader path: " + resource);
            resRoot = resource.toURI();
        } catch (URISyntaxException e) {
        }
    }

    public void clear() {
        recordEditsFound = false;
        configurationFile = null;
        setReportDirectory(null);
        if(this.eventManager != null) {
            this.eventManager.setEntityManager(entityManager);
        }

        loadConfiguration("<res>/inputs/autoload.cfg");
    }

    private String getReportDirectory() {
        if (reportDir != null)
            return reportDir.getPath() + File.separator;

        if (configurationFile != null)
            return configurationFile.getParentFile().getPath() + File.separator;

        return null;
    }

    public void setReportDirectory(File dir) {
        reportDir = dir;
        if (reportDir == null)
            return;
        if (!reportDir.exists() && !reportDir.mkdirs())
            throw new InputErrorException("Was unable to create the Report Directory: %s", reportDir.toString());
    }

    public String getReportFileName(String name) {
        return getReportDirectory() + name;
    }

    public void prepareReportDirectory() {
        if (reportDir != null) reportDir.mkdirs();
    }

    /**
     * Returns the present configuration file.
     * <p/>
     * Null is returned if no configuration file has been loaded or saved yet.
     * <p/>
     *
     * @return the present configuration file.
     */
    public File getConfigurationFile() {
        return configurationFile;
    }

    /**
     * Sets the present configuration file.
     *
     * @param file - the present configuration file.
     */
    public void setConfigurationFile(File file) {
        configurationFile = file;
    }

    /**
     * Returns the name of the simulation run.
     * <p/>
     * For example, if the configuration file name is "case1.cfg", then the
     * run name is "case1".
     * <p/>
     *
     * @return the name of simulation run.
     */
    public String getRunName() {

        if (getConfigurationFile() == null)
            return "";

        String name = getConfigurationFile().getName();
        int index = name.lastIndexOf(".");
        if (index == -1)
            return name;

        return name.substring(0, index);
    }

    /**
     * Returns the "RecordEdits" mode for the InputAgent.
     * <p/>
     * When RecordEdits is TRUE, any model inputs that are changed and any objects that
     * are defined are marked as "edited". When FALSE, model inputs and object
     * definitions are marked as "unedited".
     * <p/>
     * RecordEdits mode is used to determine the way JaamSim saves a configuration file
     * through the graphical user interface. Object definitions and model inputs
     * that are marked as unedited will be copied exactly as they appear in the original
     * configuration file that was first loaded.  Object definitions and model inputs
     * that are marked as edited will be generated automatically by the program.
     *
     * @return the RecordEdits mode for the InputAgent.
     */
    public boolean recordEdits() {
        return recordEdits;
    }

    /**
     * Sets the "RecordEdits" mode for the InputAgent.
     * <p/>
     * When RecordEdits is TRUE, any model inputs that are changed and any objects that
     * are defined are marked as "edited". When FALSE, model inputs and object
     * definitions are marked as "unedited".
     * <p/>
     * RecordEdits mode is used to determine the way JaamSim saves a configuration file
     * through the graphical user interface. Object definitions and model inputs
     * that are marked as unedited will be copied exactly as they appear in the original
     * configuration file that was first loaded.  Object definitions and model inputs
     * that are marked as edited will be generated automatically by the program.
     *
     * @param b - boolean value for the RecordEdits mode
     */
    public void setRecordEdits(boolean b) {
        recordEdits = b;
    }

    private int getBraceDepth(ArrayList<String> tokens, int startingBraceDepth, int startingIndex) {
        int braceDepth = startingBraceDepth;
        for (int i = startingIndex; i < tokens.size(); i++) {
            String token = tokens.get(i);

            if (token.equals("{"))
                braceDepth++;

            if (token.equals("}"))
                braceDepth--;

            if (braceDepth < 0) {
                LOG.error("Extra closing braces found: %s", tokens);
                tokens.clear();
            }

            if (braceDepth > 3) {
                LOG.error("Maximum brace depth (3) exceeded: %s", tokens);
                tokens.clear();
            }
        }

        return braceDepth;
    }

    private static void rethrowWrapped(Exception ex) {
        StringBuilder causedStack = new StringBuilder();
        for (StackTraceElement elm : ex.getStackTrace()) {
            causedStack.append(elm.toString()).append("\n");
        }
        throw new InputErrorException("Caught exception: %s", ex.getMessage() + "\n" + causedStack.toString());
    }

    public final void loadConfiguration(String configuration) {
        if (configuration == null)
            return;

        try {
            readStream(null, null, configuration);
            // TODO in case notify event GUIFrame.instance().setProgressText(null);
        } catch (InputErrorException ex) {
            rethrowWrapped(ex);
        }

    }

    public final void loadConfiguration(byte[] configuration) {
        if (configuration == null)
            return;

        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(configuration);

            readStream(inputStream, null);
        } catch (IOException e) {
            LOG.error("Could not read configuration");
            LOG.error(e);
        } catch (InputErrorException ex) {
            rethrowWrapped(ex);
        }

    }

    private final void readStream(InputStream in, ParseContext parseContext) throws IOException {
        LOG.debug("Reading configuration stream ...");

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in))) {
            ArrayList<String> tokens = new ArrayList<>();
            int braceDepth = 0;

            String line;
            while ((line = bufferedReader.readLine()) != null) {

                int previousRecordSize = tokens.size();
                Parser.tokenize(tokens, line, true);
                braceDepth = getBraceDepth(tokens, braceDepth, previousRecordSize);
                if (braceDepth != 0)
                    continue;

                if (tokens.size() == 0)
                    continue;

                switch (tokens.get(0).toUpperCase()) {
                    case RECORD_DEFINE:
                        processDefineRecord(tokens);
                        break;
                    case RECORD_INCLUDE:
                        try {
                            processIncludeRecord(parseContext, tokens);
                        } catch (URISyntaxException ex) {
                            rethrowWrapped(ex);
                        }
                        break;
                    case RECORD_EDITS:
                        setRecordEdits(true);
                        break;
                    default:
                        processKeywordRecord(tokens, parseContext);
                }

                tokens.clear();
            }

            // Leftover Input at end of file
            if (tokens.size() > 0)
                LOG.error("Leftover input at end of file: %s", tokens);

        } catch (IOException e) {
            throw e;
        }
        LOG.debug("Reading configuration stream ... done!");
    }

    private final void readStream(String root, URI path, String file) throws InputErrorException {
        String shortName = file.substring(file.lastIndexOf('/') + 1, file.length());
        // TODO in case notify event GUIFrame.instance().setProgressText(shortName);

        URL url = null;
        try {
            URI resolved = getFileURI(path, file, root);

            url = resolved.normalize().toURL();
            if (url == null) {
                LOG.error("Unable to resolve path %s%s - %s", root, path.toString(), file);
                return;
            }

            ParseContext parseContext = new ParseContext(resolved, root);

            InputStream in = url.openStream();
            readStream(in, parseContext);
        } catch (MalformedURLException e) {
            rethrowWrapped(e);
        } catch (IOException e) {
            LOG.error("Could not read from %s", url.toString());
            LOG.error(e);
        }
    }

    private void processIncludeRecord(ParseContext parseContext, ArrayList<String> record) throws URISyntaxException {
        if (record.size() != 2) {
            LOG.error("Bad Include record, should be: Include <File>");
            return;
        }
        readStream(parseContext.jail, parseContext.context, record.get(1).replaceAll("\\\\", "/"));
    }

    private void processDefineRecord(List<String> record) {
        if (record.size() < 5 || !record.get(2).equals("{") || !record.get(record.size() - 1).equals("}")) {
            LOG.error("Bad Define record, should be: Define <Type> { <names>... }");
            return;
        }

        // TODO move responsibility
        // get entity's type
        Class<? extends Entity> prototype = null;
        try {
            prototype = entityManager.getEntityType(record.get(1));
        } catch (InputErrorException e) {
            LOG.error("%s", e.getMessage());
            return;
        }

        // Loop over all the new Entity names
        for (int i = 3; i < record.size() - 1; i++) {
            // get entity's instance
            Entity entity = entityManager.defineEntity(prototype, record.get(i), recordEdits());
            entity.setConfigurationManager(this);
        }
    }

    public void processKeywordRecord(ArrayList<String> record, ParseContext context) {
        Entity entity = entityManager.getEntity(record.get(0), Entity.class);
        if (entity == null) {
            LOG.error("Could not find Entity: %s", record.get(0));
            return;
        }

        // Validate the tokens have the Entity Keyword { Args... } Keyword { Args... }
        ArrayList<KeywordIndex> keywords = getKeywords(record, context);
        for (KeywordIndex keyword : keywords) {
            try {
                processKeyword(entity, keyword);
            } catch (Throwable e) {
                LOG.error("Entity: %s, Keyword: %s - %s", entity.getName(), keyword.keyword, e.getMessage());
                if (e.getMessage() == null) {
                    for (StackTraceElement each : e.getStackTrace())
                        LOG.info(each.toString());
                }
            }
        }
    }

    private ArrayList<KeywordIndex> getKeywords(ArrayList<String> input, ParseContext context) {
        ArrayList<KeywordIndex> ret = new ArrayList<>();

        int braceDepth = 0;
        int keyWordIdx = 1;
        for (int i = 1; i < input.size(); i++) {
            String tok = input.get(i);
            if ("{".equals(tok)) {
                braceDepth++;
                continue;
            }

            if ("}".equals(tok)) {
                braceDepth--;
                if (braceDepth == 0) {
                    // validate keyword form
                    String keyword = input.get(keyWordIdx);
                    if (keyword.equals("{") || keyword.equals("}") || !input.get(keyWordIdx + 1).equals("{"))
                        throw new InputErrorException("The input for a keyword must be enclosed by braces. Should be <keyword> { <args> }");

                    ret.add(new KeywordIndex(keyword, input, keyWordIdx + 2, i, context));
                    keyWordIdx = i + 1;
                    continue;
                }
            }
        }

        if (keyWordIdx != input.size())
            throw new InputErrorException("The input for a keyword must be enclosed by braces. Should be <keyword> { <args> }");

        return ret;
    }

    public Simulation getSimulation() {
        return entityManager.getSimulation();
    }

    public void loadThresholdUser(Threshold threshold, List<ThresholdUser> userList) {
        for(Entity entity : entityManager.getAll()) {
            if (entity instanceof ThresholdUser) {
                ThresholdUser thresholdUser = (ThresholdUser) entity;
                if (thresholdUser.getThresholds().contains(this))
                    userList.add(thresholdUser);
            }
        }

    }

    // Load the run file
    public void loadConfiguration(File file) throws InputErrorException {
        setConfigurationFile(file);
        setReportDirectory(new File(file.getParent() + File.separator + getRunName()));

        String inputTraceFileName = getRunName() + ".log";
        // Initializing the tracing for the model
        try {
            System.out.println("Creating trace file");

            URI confURI = file.toURI();
            URI logURI = confURI.resolve(new URI(null, inputTraceFileName, null)); // The new URI here effectively escapes the file name

            // Set and open the input trace file name
        } catch (Exception e) {
            LOG.warn("Could not create trace file");
        }

        URI dirURI = file.getParentFile().toURI();
        readStream("", dirURI, file.getName());

    }

    /**
     * Prepares the keyword and input value for processing.
     *
     * @param entity     - the entity whose keyword and value has been entered.
     * @param keyword - the keyword.
     * @param args    - the input value String for the keyword.
     */
    public void applyArgs(Entity entity, String keyword, String... args) {
        // Keyword
        ArrayList<String> tokens = new ArrayList<>(args.length);
        for (String each : args)
            tokens.add(each);

        // Parse the keyword inputs
        KeywordIndex keywordIndex = new KeywordIndex(keyword, tokens, null);
        apply(entity, keywordIndex);
    }

    public final void apply(Entity entity, KeywordIndex keywordIndex) {
        Input<?> input = entity.getInput(keywordIndex.keyword);
        if (input == null) {
            LOG.error("Keyword %s could not be found for Entity %s.", keywordIndex.keyword, entity.getName());
            return;
        }

        input.setEntityManager(entityManager);

        apply(entity, input, keywordIndex);
        // TODO in case notify update FrameBox.valueUpdate();
    }

    public final void apply(Entity entity, Input<?> input, KeywordIndex keywordIndex) {
        // If the input value is blank, restore the default
        if (keywordIndex.numArgs() == 0) {
            input.reset();
        } else {
            input.parse(keywordIndex);
            input.setTokens(keywordIndex);
        }

        // Only mark the keyword edited if we have finished initial configuration
        if (recordEdits()) {
            input.setEdited(true);
            entity.setFlag(Entity.FLAG_EDITED);
        }

        entity.updateForInput(input);
        // TODO responsibility to be moved
        // entityManager.addEntity(entity);
    }

    public void processKeyword(Entity entity, KeywordIndex key) {
        if (entity.testFlag(Entity.FLAG_LOCKED))
            throw new InputErrorException("Entity: %s is locked and cannot be modified", entity.getName());

        Input<?> input = entity.getInput(key.keyword);
        input.setEntityManager(entityManager);
        if (input != null) {
            apply(entity, input, key);
            // TODO in case notify update FrameBox.valueUpdate();
            return;
        }

        if (!(entity instanceof Group))
            throw new InputErrorException("Not a valid keyword");

        Group grp = (Group) entity;
        grp.saveGroupKeyword(key);

        // Store the keyword data for use in the edit table
        for (int i = 0; i < grp.getList().size(); i++) {
            Entity ent = grp.getList().get(i);
            apply(ent, key);
        }
    }

    /**
     * Returns the relative file path for the specified URI.
     * <p/>
     * The path can start from either the folder containing the present
     * configuration file or from the resources folder.
     * <p/>
     *
     * @param uri - the URI to be relativized.
     * @return the relative file path.
     */
    public String getRelativeFilePath(URI uri) {

        // Relativize the file path against the resources folder
        String resString = resRoot.toString();
        String inputString = uri.toString();
        if (inputString.startsWith(resString)) {
            return String.format("<res>/%s", inputString.substring(resString.length()));
        }

        // Relativize the file path against the configuration file
        try {
            URI configDirURI = getConfigurationFile().getParentFile().toURI();
            return String.format("%s", configDirURI.relativize(uri).getPath());
        } catch (Exception ex) {
            return String.format("%s", uri.getPath());
        }
    }

    /**
     * Loads the default configuration file.
     */
    public void loadDefault() {
    	LOG.info("Loading default configuration");

        // Read the default configuration file
        loadConfiguration("<res>/inputs/default.cfg");
    }

    public void shutdown() {
        getSimulation().clear();
        if(eventManager != null) {
            eventManager.stop();
        }
        entityManager.killEntities();
        entityManager = null;
        eventManager = null;
    }

    public static KeywordIndex formatPointsInputs(String keyword, ArrayList<Vec3d> points, Vec3d offset) {
        ArrayList<String> tokens = new ArrayList<>(points.size() * 6);
        for (Vec3d v : points) {
            tokens.add("{");
            tokens.add(String.format((Locale) null, "%.3f", v.x + offset.x));
            tokens.add(String.format((Locale) null, "%.3f", v.y + offset.y));
            tokens.add(String.format((Locale) null, "%.3f", v.z + offset.z));
            tokens.add("m");
            tokens.add("}");
        }

        // Parse the keyword inputs
        return new KeywordIndex(keyword, tokens, null);
    }

    public static KeywordIndex formatPointInputs(String keyword, Vec3d point, String unit) {
        ArrayList<String> tokens = new ArrayList<>(4);
        tokens.add(String.format((Locale) null, "%.6f", point.x));
        tokens.add(String.format((Locale) null, "%.6f", point.y));
        tokens.add(String.format((Locale) null, "%.6f", point.z));
        if (unit != null)
            tokens.add(unit);

        // Parse the keyword inputs
        return new KeywordIndex(keyword, tokens, null);
    }

    /**
     * Split an input (list of strings) down to a single level of nested braces, this may then be called again for
     * further nesting.
     *
     * @param input
     * @return
     */
    public static ArrayList<ArrayList<String>> splitForNestedBraces(List<String> input) {
        ArrayList<ArrayList<String>> inputs = new ArrayList<>();

        int braceDepth = 0;
        ArrayList<String> currentLine = null;
        for (int i = 0; i < input.size(); i++) {
            if (currentLine == null)
                currentLine = new ArrayList<>();

            currentLine.add(input.get(i));
            if (input.get(i).equals("{")) {
                braceDepth++;
                continue;
            }

            if (input.get(i).equals("}")) {
                braceDepth--;
                if (braceDepth == 0) {
                    inputs.add(currentLine);
                    currentLine = null;
                    continue;
                }
            }
        }

        return inputs;
    }

    /**
     * Converts a file path String to a URI.
     * <p/>
     * The specified file path can be either relative or absolute. In the case
     * of a relative file path, a 'context' folder must be specified. A context
     * of null indicates an absolute file path.
     * <p/>
     * To avoid bad input accessing an inappropriate file, a 'jail' folder can
     * be specified. The URI to be returned must include the jail folder for it
     * to be valid.
     * <p/>
     *
     * @param context    - full file path for the folder that is the reference for relative file paths.
     * @param filePath   - string to be resolved to a URI.
     * @param jailPrefix - file path to a base folder from which a relative cannot escape.
     * @return the URI corresponding to the context and filePath.
     */
    public static URI getFileURI(URI context, String filePath, String jailPrefix) throws InputErrorException {

        URI fileUri = null;

        try {
            if (filePath.startsWith(RESOURCE_TAG)) {
                fileUri = new URI(resRoot.getScheme(), resRoot.getSchemeSpecificPart() + filePath.replaceFirst(RESOURCE_TAG, ""), null).normalize();
            } else {
                URI pathURI = new URI(null, filePath, null).normalize();

                if (context != null) {
                    if (context.isOpaque()) {
                        // Things are going to get messy in here
                        URI schemeless = new URI(null, context.getSchemeSpecificPart(), null);
                        URI resolved = schemeless.resolve(pathURI).normalize();

                        // Note: we are using the one argument constructor here because the 'resolved' URI is already encoded
                        // and we do not want to double-encode (and schemes should never need encoding, I hope)
                        fileUri = new URI(context.getScheme() + ":" + resolved.toString());
                    } else {
                        fileUri = context.resolve(pathURI).normalize();
                    }
                } else {
                    // We have no context, so append a 'file' scheme if necessary
                    if (pathURI.getScheme() == null) {
                        fileUri = new URI("file", pathURI.getPath(), null);
                    } else {
                        fileUri = pathURI;
                    }
                }
            }
        } catch (URISyntaxException e) {
            rethrowWrapped(e);
        }

        // Check that the file path includes the jail folder
        if (jailPrefix != null && fileUri.toString().indexOf(jailPrefix) != 0) {
            // TODO in case notify event LogBox.getInstance().setVisible(true);
            rethrowWrapped(new InputErrorException(String.format("Failed jail test: %s\njail: %s\ncontext: %s\n",
                    fileUri.toString(), jailPrefix, context.toString())));
        }

        return fileUri;
    }

    /**
     * Determines whether or not a file exists.
     * <p/>
     *
     * @param filePath - URI for the file to be tested.
     * @return true if the file exists, false if it does not.
     */
    public static boolean fileExists(URI filePath) {

        try {
            InputStream in = filePath.toURL().openStream();
            in.close();
            return true;
        } catch (MalformedURLException ex) {
            return false;
        } catch (IOException ex) {
            return false;
        }
    }
}
