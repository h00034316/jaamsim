/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2010-2012 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.input;

import com.jaamsim.Samples.*;
import com.jaamsim.basicsim.Entity;
import com.jaamsim.basicsim.EntityManager;
import com.jaamsim.basicsim.Group;
import com.jaamsim.basicsim.ObjectType;
import com.jaamsim.datatypes.BooleanVector;
import com.jaamsim.datatypes.DoubleVector;
import com.jaamsim.datatypes.IntegerVector;
import com.jaamsim.input.ExpParser.Expression;
import com.jaamsim.math.Color4d;
import com.jaamsim.units.DimensionlessUnit;
import com.jaamsim.units.TimeUnit;
import com.jaamsim.units.Unit;
import com.jaamsim.units.UserSpecifiedUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public abstract class Input<T> {

    private static final Logger LOG = LogManager.getFormatterLogger(Input.class.getSimpleName());

    public static final String POSITIVE_INFINITY = "Infinity";
    public static final String NEGATIVE_INFINITY = "-Infinity";
    protected static final String INP_ERR_COUNT = "Expected an input with %s value(s), received: %s";
    protected static final String INP_ERR_RANGECOUNT = "Expected an input with %d to %d values, received: %s";
    protected static final String INP_ERR_RANGECOUNTMIN = "Expected an input with at least %d values, received: %s";
    protected static final String INP_ERR_EVENCOUNT = "Expected an input with even number of values, received: %s";
    protected static final String INP_ERR_ODDCOUNT = "Expected an input with odd number of values, received: %s";
    protected static final String INP_ERR_BOOLEAN = "Expected a boolean value, received: %s";
    protected static final String INP_ERR_INTEGER = "Expected an integer value, received: %s";
    protected static final String INP_ERR_INTEGERRANGE = "Expected an integer between %d and %d, received: %d";
    protected static final String INP_ERR_DOUBLE = "Expected an numeric value, received: %s";
    protected static final String INP_ERR_DOUBLERANGE = "Expected a number between %f and %f, received: %f";
    protected static final String INP_ERR_TIME = "Expected a time value (hh:mm or hh:mm:ss), received: %s";
    protected static final String INP_ERR_TIMEVALUE = "Expected a numeric value, 12 numeric values, or a probabilty distribution, received: %s";
    protected static final String INP_ERR_BADSUM = "List must sum to %f, received:%f";
    protected static final String INP_ERR_BADCHOICE = "Expected one of %s, received: %s";
    protected static final String INP_ERR_ELEMENT = "Error parsing element %d: %s";
    protected static final String INP_ERR_ENTNAME = "Could not find an Entity named: %s";
    protected static final String INP_ERR_NOUNITFOUND = "A unit is required, could not parse '%s' as a %s";
    protected static final String INP_ERR_UNITNOTFOUND = "A unit of type '%s' is required";
    protected static final String INP_ERR_NOTUNIQUE = "List must contain unique entries, repeated entry: %s";
    protected static final String INP_ERR_NOTVALIDENTRY = "List must not contain: %s";
    protected static final String INP_ERR_ENTCLASS = "Expected a %s, %s is a %s";
    protected static final String INP_ERR_INTERFACE = "Expected an object implementing %s, %s does not";
    protected static final String INP_ERR_UNITS = "Unit types do not match";
    protected static final String INP_ERR_UNITUNSPECIFIED = "Unit type has not been specified";
    protected static final String INP_ERR_NOTSUBCLASS = "Expected a subclass of %s, got %s";
    protected static final String INP_ERR_BADDATE = "Expected a valid RFC8601 datetime, got: %s";
    protected static final String INP_VAL_LISTSET = "Values found for %s without %s being set";
    protected static final String INP_VAL_LISTSIZE = "%s and %s must be of equal size";
    protected static final String SEPARATOR = "  ";

    private static final Pattern is8601date = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
    private static final Pattern is8601time = Pattern.compile("\\d{4}-\\d{2}-\\d{2}[ T]\\d{2}:\\d{2}:\\d{2}");
    private static final Pattern is8601full = Pattern.compile("\\d{4}-\\d{2}-\\d{2}[ T]\\d{2}:\\d{2}:\\d{2}\\.\\d{1,6}");
    private static final Pattern isextendtime = Pattern.compile("\\d{1,}:\\d{2}:\\d{2}");
    private static final Pattern isextendfull = Pattern.compile("\\d{1,}:\\d{2}:\\d{2}.\\d{1,6}");

    private static final long usPerSec = 1000000;
    private static final long usPerMin = 60 * usPerSec;
    private static final long usPerHr = 60 * usPerMin;
    private static final long usPerDay = 24 * usPerHr;
    public static final long usPerYr = 365 * usPerDay;

    private static final int[] daysInMonth;
    private static final int[] firstDayOfMonth;

    static {
        daysInMonth = new int[12];
        daysInMonth[0] = 31;
        daysInMonth[1] = 28;
        daysInMonth[2] = 31;
        daysInMonth[3] = 30;
        daysInMonth[4] = 31;
        daysInMonth[5] = 30;
        daysInMonth[6] = 31;
        daysInMonth[7] = 31;
        daysInMonth[8] = 30;
        daysInMonth[9] = 31;
        daysInMonth[10] = 30;
        daysInMonth[11] = 31;

        firstDayOfMonth = new int[12];
        firstDayOfMonth[0] = 1;
        for (int i = 1; i < firstDayOfMonth.length; i++) {
            firstDayOfMonth[i] = firstDayOfMonth[i - 1] + daysInMonth[i - 1];
        }
    }

    private final String category;
    protected T defaultValue;
    protected T value;
    private String keyword; // the preferred name for the input keyword
    private boolean edited; // indicates if input has been edited for this entity
    private boolean promptRequired; // indicates whether to prompt the user to save the configuration file
    private boolean hidden; // Hide this input from the EditBox
    private boolean isDefault; // Is this input still the default value?
    private String[] valueTokens; // value from .cfg file
    private String defaultText; // special text to show in the default column of the Input Editor
    private boolean required;     // indicates whether this input must be provided by the user

    protected EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public Input(String keyword, String category, T defaultValue) {
        this.keyword = keyword;
        this.category = category;
        setDefaultValue(defaultValue);

        edited = false;
        promptRequired = true;
        isDefault = true;
        hidden = false;
        valueTokens = null;
        defaultText = null;
        required = false;
    }

    // TODO static
    public static void assertCount(DoubleVector input, int... counts) throws InputErrorException {
        // If there is no constraint on the element count, return
        if (counts == null || counts.length == 0)
            return;

        // If there is an exact constraint, check the count
        for (int each : counts) {
            if (each == input.size())
                return;
        }

        // Input size is not equal to any of the specified counts
        throw new InputErrorException(INP_ERR_COUNT, Arrays.toString(counts), input.toString());
    }

    /**
     * Verifies that the correct number of inputs have been provided.
     *
     * @param keywordIndex     - object containing the inputs.
     * @param counts - list of valid numbers of inputs. All other numbers are invalid.
     * @throws InputErrorException
     */
    // TODO static
    public static void assertCount(KeywordIndex keywordIndex, int... counts) throws InputErrorException {
        // If there is no constraint on the element count, return
        if (counts.length == 0)
            return;

        // If there is an exact constraint, check the count
        for (int each : counts) {
            if (each == keywordIndex.numArgs())
                return;
        }

        // Input size is not equal to any of the specified counts
        if (counts.length == 1)
            throw new InputErrorException(INP_ERR_COUNT, counts[0], keywordIndex.argString());
        else {
            StringBuilder sb = new StringBuilder();
            sb.append(counts[0]);
            for (int i = 1; i < counts.length - 1; i++) {
                sb.append(", ").append(counts[i]);
            }
            sb.append(" or ").append(counts[counts.length - 1]);
            throw new InputErrorException(INP_ERR_COUNT, sb.toString(), keywordIndex.argString());
        }
    }

    /**
     * Verifies that the correct number of inputs have been provided.
     *
     * @param keywordIndex  - object containing the inputs.
     * @param min - minimum number of inputs that are valid
     * @param max - maximum number of inputs that are valid
     * @throws InputErrorException
     */
    public static void assertCountRange(KeywordIndex keywordIndex, int min, int max) throws InputErrorException {
        // For a range with a single value, fall back to the exact test
        if (min == max) {
            assertCount(keywordIndex, min);
            return;
        }

        if (keywordIndex.numArgs() < min || keywordIndex.numArgs() > max) {
            if (max == Integer.MAX_VALUE)
                throw new InputErrorException(INP_ERR_RANGECOUNTMIN, min, keywordIndex.argString());
            throw new InputErrorException(INP_ERR_RANGECOUNT, min, max, keywordIndex.argString());
        }
    }

    // TODO static
    public static void assertCount(List<String> input, int... counts) throws InputErrorException {
        // If there is no constraint on the element count, return
        if (counts.length == 0)
            return;

        // If there is an exact constraint, check the count
        for (int each : counts) {
            if (each == input.size())
                return;
        }

        // Input size is not equal to any of the specified counts
        throw new InputErrorException(INP_ERR_COUNT, Arrays.toString(counts), input.toString());
    }

    // TODO static
    public static void assertCountRange(DoubleVector input, int min, int max) throws InputErrorException {
        // For a range with a single value, fall back to the exact test
        if (min == max) {
            assertCount(input, min);
            return;
        }

        if (input.size() < min || input.size() > max) {
            if (max == Integer.MAX_VALUE)
                throw new InputErrorException(INP_ERR_RANGECOUNTMIN, min, input.toString());
            throw new InputErrorException(INP_ERR_RANGECOUNT, min, max, input.toString());
        }
    }

    // TODO static
    public static void assertCountRange(List<String> input, int min, int max) throws InputErrorException {
        // For a range with a single value, fall back to the exact test
        if (min == max) {
            assertCount(input, min);
            return;
        }

        if (input.size() < min || input.size() > max) {
            if (max == Integer.MAX_VALUE)
                throw new InputErrorException(INP_ERR_RANGECOUNTMIN, min, input.toString());
            throw new InputErrorException(INP_ERR_RANGECOUNT, min, max, input.toString());
        }
    }

    // TODO static
    public static void assertCountEven(KeywordIndex keywordIndex) throws InputErrorException {
        if ((keywordIndex.numArgs() % 2) != 0)
            throw new InputErrorException(INP_ERR_EVENCOUNT, keywordIndex.argString());
    }

    // TODO static
    public static void assertCountOdd(KeywordIndex keywordIndex) throws InputErrorException {
        if ((keywordIndex.numArgs() % 2) == 0)
            throw new InputErrorException(INP_ERR_ODDCOUNT, keywordIndex.argString());
    }

    // TODO static
    public static <T extends Entity> void assertNotPresent(ArrayList<? super T> list, T entity) throws InputErrorException {
        if (list.contains(entity))
            throw new InputErrorException(INP_ERR_NOTVALIDENTRY, entity.getName());
    }

    // TODO static
    public static void assertSumTolerance(DoubleVector vector, double sum, double tolerance) throws InputErrorException {
        // Vector sum is within tolerance of given sum, no error
        if (Math.abs(vector.sum() - sum) < tolerance)
            return;

        throw new InputErrorException(INP_ERR_BADSUM, sum, vector.sum());
    }

    // TODO check static
    public <T> T parse(List<String> data, Class<T> aClass, String units, double minValue, double maxValue, int minCount, int maxCount, Class<? extends Unit> unitType) {

        if (aClass == Double.class) {
            if (units != null)
                return aClass.cast(parseDouble(data, minValue, maxValue, units));
            else {
                DoubleVector tmp = parseDoubles(data, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, unitType);
                assertCount(tmp, 1);
                return aClass.cast(tmp.get(0));
            }
        }

        if (aClass == DoubleVector.class) {
            if (units != null) {
                DoubleVector value = parseDoubleVector(data, minValue, maxValue, units);
                if (value.size() < minCount || value.size() > maxCount) {
                    if (maxCount == Integer.MAX_VALUE)
                        throw new InputErrorException(INP_ERR_RANGECOUNTMIN, minCount, data);
                    throw new InputErrorException(INP_ERR_RANGECOUNT, minCount, maxCount, data);
                }
                return aClass.cast(value);
            } else {
                DoubleVector tmp = parseDoubles(data, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, unitType);
                return aClass.cast(tmp);
            }
        }

        if (Entity.class.isAssignableFrom(aClass)) {
            Class<? extends Entity> temp = aClass.asSubclass(Entity.class);
            assertCount(data, 1, 1);
            return aClass.cast(parseEntity(data.get(0), temp));
        }

        if (aClass == Boolean.class) {
            Input.assertCount(data, 1);
            Boolean value = Boolean.valueOf(parseBoolean(data.get(0)));
            return aClass.cast(value);
        }

        if (aClass == Integer.class) {
            Input.assertCount(data, 1);
            Integer value = parseInteger(data.get(0), (int) minValue, (int) maxValue);
            return aClass.cast(value);
        }

        if (aClass == SampleProvider.class) {

            // Try to parse as a constant value
            try {
                DoubleVector tmp = parseDoubles(data, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, unitType);
                assertCount(tmp, 1);
                return aClass.cast(new SampleConstant(unitType, tmp.get(0)));
            } catch (InputErrorException e) {
            }

            // If not a constant, try parsing a SampleProvider
            assertCount(data, 1);
            Entity entity = parseEntity(data.get(0), Entity.class);
            SampleProvider sampleProvider = castImplements(entity, SampleProvider.class);
            if (sampleProvider.getUnitType() != UserSpecifiedUnit.class)
                assertEquals(unitType, sampleProvider.getUnitType());
            return aClass.cast(sampleProvider);
        }

        if (aClass == IntegerVector.class) {
            IntegerVector value = parseIntegerVector(data, (int) minValue, (int) maxValue);
            if (value.size() < minCount || value.size() > maxCount)
                throw new InputErrorException(INP_ERR_RANGECOUNT, minCount, maxCount, data);
            return aClass.cast(value);
        }

        // TODO - parse other classes
        throw new InputErrorException("%s is not supported for parsing yet", aClass);
    }

    /**
     * Converts a file path entry in a configuration file to a URI.
     *
     * @param keywordIndex - keyword input containing the file path data
     * @return the URI corresponding to the file path data.
     * @throws InputErrorException
     */
    // TODO static
    public static URI parseURI(KeywordIndex keywordIndex) throws InputErrorException {
        assertCount(keywordIndex, 1);

        String arg = keywordIndex.getArg(0);

        // Convert the file path to a URI
        URI uri = null;
        try {
            if (keywordIndex.context != null)
                uri = ConfigurationManager.getFileURI(keywordIndex.context.context, arg, keywordIndex.context.jail);
            else
                uri = ConfigurationManager.getFileURI(null, arg, null);
        } catch (InputErrorException ex) {
            throw new InputErrorException("File Entity parse error: %s", ex.getMessage());
        }

        if (uri == null)
            throw new InputErrorException("Unable to parse the file path:\n%s", arg);

        if (!uri.isOpaque() && uri.getPath() == null)
            throw new InputErrorException("Unable to parse the file path:\n%s", arg);

        return uri;
    }

    // TODO static
    public static boolean parseBoolean(String value) throws InputErrorException {
        if ("TRUE".equals(value)) {
            return true;
        }

        if ("FALSE".equals(value)) {
            return false;
        }

        throw new InputErrorException(INP_ERR_BOOLEAN, value);
    }

    // TODO static
    public static BooleanVector parseBooleanVector(KeywordIndex keywordIndex) throws InputErrorException {
        BooleanVector temp = new BooleanVector(keywordIndex.numArgs());

        for (int i = 0; i < keywordIndex.numArgs(); i++) {
            try {
                boolean element = parseBoolean(keywordIndex.getArg(i));
                temp.add(element);
            } catch (InputErrorException e) {
                throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
            }
        }
        return temp;
    }

    // TODO static
    public static BooleanVector parseBooleanVector(List<String> input) throws InputErrorException {
        BooleanVector temp = new BooleanVector(input.size());

        for (int i = 0; i < input.size(); i++) {
            try {
                boolean element = parseBoolean(input.get(i));
                temp.add(element);
            } catch (InputErrorException e) {
                throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
            }
        }
        return temp;
    }

    public ArrayList<Color4d> parseColorVector(KeywordIndex keywordIndex) throws InputErrorException {
        ArrayList<KeywordIndex> subArgs = keywordIndex.getSubArgs();
        ArrayList<Color4d> temp = new ArrayList<>(subArgs.size());

        for (int i = 0; i < subArgs.size(); i++) {
            try {
                Color4d element = parseColour(subArgs.get(i));
                temp.add(element);
            } catch (InputErrorException e) {
                throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
            }
        }
        return temp;
    }

    // TODO static
    public static Class<? extends Entity> parseClass(String data) {
        Class<? extends Entity> entProto = null;
        try {
            Class<?> proto = Class.forName(data);
            entProto = proto.asSubclass(Entity.class);
        } catch (ClassNotFoundException e) {
            throw new InputErrorException("Class not found " + data);
        }
        return entProto;
    }

    public static int parseInteger(String data)
            throws InputErrorException {
        return Input.parseInteger(data, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    // TODO static
    public static int parseInteger(String data, int minValue, int maxValue) throws InputErrorException {
        int temp;
        try {
            temp = Integer.parseInt(data);
        } catch (NumberFormatException e) {
            throw new InputErrorException(INP_ERR_INTEGER, data);
        }

        if (temp < minValue || temp > maxValue)
            throw new InputErrorException(INP_ERR_INTEGERRANGE, minValue, maxValue, temp);

        return temp;
    }

    // TODO static
    public static boolean isInteger(String val) {
        try {
            Integer.parseInt(val);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    // TODO static
    public static boolean isDouble(String val) {
        try {
            Double.parseDouble(val);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    // TODO static
    public static IntegerVector parseIntegerVector(List<String> input, int minValue, int maxValue) throws InputErrorException {
        IntegerVector temp = new IntegerVector(input.size());

        for (int i = 0; i < input.size(); i++) {
            try {
                int element = parseInteger(input.get(i), minValue, maxValue);
                temp.add(element);
            } catch (InputErrorException e) {
                throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
            }
        }
        return temp;
    }

    public static IntegerVector parseIntegerVector(KeywordIndex keywordIndex, int minValue, int maxValue) throws InputErrorException {
        IntegerVector temp = new IntegerVector(keywordIndex.numArgs());

        for (int i = 0; i < keywordIndex.numArgs(); i++) {
            try {
                int element = parseInteger(keywordIndex.getArg(i), minValue, maxValue);
                temp.add(element);
            } catch (InputErrorException e) {
                throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
            }
        }
        return temp;
    }

    // TODO static
    public static double parseTime(String data, double minValue, double maxValue) throws InputErrorException {
        return parseTime(data, minValue, maxValue, 1.0);
    }

    /**
     * Convert the given String to a double and apply the given conversion factor
     */
    // TODO static
    public static double parseTime(String data, double minValue, double maxValue, double factor) throws InputErrorException {
        double value = 0.0d;

        // check for hh:mm:ss or hh:mm
        if (data.indexOf(":") > -1) {
            String[] splitDouble = data.split(":");
            if (splitDouble.length != 2 && splitDouble.length != 3)
                throw new InputErrorException(INP_ERR_TIME, data);

            try {
                double hour = Double.valueOf(splitDouble[0]);
                double min = Double.valueOf(splitDouble[1]);
                double sec = 0.0d;

                if (splitDouble.length == 3)
                    sec = Double.valueOf(splitDouble[2]);

                value = hour + (min / 60.0d) + (sec / 3600.0d);
            } catch (NumberFormatException e) {
                throw new InputErrorException(INP_ERR_TIME, data);
            }
        } else {
            value = parseDouble(data);
        }
        value = value * factor;

        if (value < minValue || value > maxValue)
            throw new InputErrorException(INP_ERR_DOUBLERANGE, minValue, maxValue, value);

        return value;
    }

    // TODO static
    public static boolean isRFC8601DateTime(String input) {
        if (is8601time.matcher(input).matches()) return true;
        if (is8601full.matcher(input).matches()) return true;
        if (is8601date.matcher(input).matches()) return true;
        if (isextendtime.matcher(input).matches()) return true;
        if (isextendfull.matcher(input).matches()) return true;
        return false;
    }

    /**
     * Parse an RFC8601 date time and return it as an offset in microseconds from
     * 0AD. This assumes a very simple concept of a 365 day year with no leap years
     * and no leap seconds.
     * <p/>
     * An RFC8601 date time looks like YYYY-MM-DD HH:MM:SS.mmm or YYYY-MM-DDTHH:MM:SS.mmm
     *
     * @param input
     * @return
     */
    // TODO static
    public static long parseRFC8601DateTime(String input) {
        if (is8601time.matcher(input).matches()) {
            int YY = Integer.parseInt(input.substring(0, 4));
            int MM = Integer.parseInt(input.substring(5, 7));
            int DD = Integer.parseInt(input.substring(8, 10));
            int hh = Integer.parseInt(input.substring(11, 13));
            int mm = Integer.parseInt(input.substring(14, 16));
            int ss = Integer.parseInt(input.substring(17, 19));
            return getUS(input, YY, MM, DD, hh, mm, ss, 0);
        }

        if (is8601full.matcher(input).matches()) {
            int YY = Integer.parseInt(input.substring(0, 4));
            int MM = Integer.parseInt(input.substring(5, 7));
            int DD = Integer.parseInt(input.substring(8, 10));
            int hh = Integer.parseInt(input.substring(11, 13));
            int mm = Integer.parseInt(input.substring(14, 16));
            int ss = Integer.parseInt(input.substring(17, 19));

            // grab the us values and zero-pad to a full 6-digit number
            String usChars = input.substring(20, input.length());
            int us = 0;
            switch (usChars.length()) {
                case 1:
                    us = Integer.parseInt(usChars) * 100000;
                    break;
                case 2:
                    us = Integer.parseInt(usChars) * 10000;
                    break;
                case 3:
                    us = Integer.parseInt(usChars) * 1000;
                    break;
                case 4:
                    us = Integer.parseInt(usChars) * 100;
                    break;
                case 5:
                    us = Integer.parseInt(usChars) * 10;
                    break;
                case 6:
                    us = Integer.parseInt(usChars) * 1;
                    break;
            }
            return getUS(input, YY, MM, DD, hh, mm, ss, us);
        }

        if (is8601date.matcher(input).matches()) {
            int YY = Integer.parseInt(input.substring(0, 4));
            int MM = Integer.parseInt(input.substring(5, 7));
            int DD = Integer.parseInt(input.substring(8, 10));
            return getUS(input, YY, MM, DD, 0, 0, 0, 0);
        }

        if (isextendtime.matcher(input).matches()) {
            int len = input.length();
            int hh = Integer.parseInt(input.substring(0, len - 6));
            int mm = Integer.parseInt(input.substring(len - 5, len - 3));
            int ss = Integer.parseInt(input.substring(len - 2, len));

            if (mm < 0 || mm > 59 || ss < 0 || ss > 59)
                throw new InputErrorException(INP_ERR_BADDATE, input);

            long ret = 0;
            ret += hh * usPerHr;
            ret += mm * usPerMin;
            ret += ss * usPerSec;
            return ret;
        }

        if (isextendfull.matcher(input).matches()) {
            int len = input.indexOf(".");
            int hh = Integer.parseInt(input.substring(0, len - 6));
            int mm = Integer.parseInt(input.substring(len - 5, len - 3));
            int ss = Integer.parseInt(input.substring(len - 2, len));

            if (mm < 0 || mm > 59 || ss < 0 || ss > 59)
                throw new InputErrorException(INP_ERR_BADDATE, input);

            // grab the us values and zero-pad to a full 6-digit number
            String usChars = input.substring(len + 1, input.length());
            int us = 0;
            switch (usChars.length()) {
                case 1:
                    us = Integer.parseInt(usChars) * 100000;
                    break;
                case 2:
                    us = Integer.parseInt(usChars) * 10000;
                    break;
                case 3:
                    us = Integer.parseInt(usChars) * 1000;
                    break;
                case 4:
                    us = Integer.parseInt(usChars) * 100;
                    break;
                case 5:
                    us = Integer.parseInt(usChars) * 10;
                    break;
                case 6:
                    us = Integer.parseInt(usChars) * 1;
                    break;
            }
            long ret = 0;
            ret += hh * usPerHr;
            ret += mm * usPerMin;
            ret += ss * usPerSec;
            ret += us;
            return ret;
        }

        throw new InputErrorException(INP_ERR_BADDATE, input);
    }

    // TODO static
    private static final long getUS(String input, int YY, int MM, int DD, int hh, int mm, int ss, int us) {
        // Validate ranges
        if (MM <= 0 || MM > 12)
            throw new InputErrorException(INP_ERR_BADDATE, input);

        if (DD <= 0 || DD > daysInMonth[MM - 1])
            throw new InputErrorException(INP_ERR_BADDATE, input);

        if (hh < 0 || hh > 23)
            throw new InputErrorException(INP_ERR_BADDATE, input);

        if (mm < 0 || mm > 59 || ss < 0 || ss > 59)
            throw new InputErrorException(INP_ERR_BADDATE, input);

        long ret = 0;
        ret += YY * usPerYr;
        ret += (firstDayOfMonth[MM - 1] - 1) * usPerDay;
        ret += (DD - 1) * usPerDay;
        ret += hh * usPerHr;
        ret += mm * usPerMin;
        ret += ss * usPerSec;
        ret += us;

        return ret;
    }

    // TODO static
    public static double parseDouble(String data) throws InputErrorException {
        return parseDouble(data, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    }

    // TODO static
    public static double parseDouble(String data, double minValue, double maxValue) throws InputErrorException {
        return parseDouble(data, minValue, maxValue, 1.0);
    }

    /**
     * Convert the given String to a double and apply the given conversion factor
     */
    // TODO static
    public static double parseDouble(String data, double minValue, double maxValue, double factor) throws InputErrorException {
        double temp;
        try {
            temp = Double.parseDouble(data) * factor;
        } catch (NumberFormatException e) {
            throw new InputErrorException(INP_ERR_DOUBLE, data);
        }

        if (temp < minValue || temp > maxValue)
            throw new InputErrorException(INP_ERR_DOUBLERANGE, minValue, maxValue, temp);

        return temp;
    }

    /**
     * Convert the given String to a double including a unit conversion, if necessary
     */
    // TODO check static
    public double parseDouble(List<String> input, double minValue, double maxValue, String defaultUnitString) throws InputErrorException {
        assertCountRange(input, 1, 2);

        // Warn if the default unit is assumed by the input data
        if (input.size() == 1 && defaultUnitString.length() > 0)
            LOG.warn("Missing units.  Assuming %s.", defaultUnitString);

        // If there are two values, then assume the last one is a unit
        double conversionFactor = 1.0;
        if (input.size() == 2) {

            // Determine the units
            Unit unit = parseUnit(input.get(1));

            // Determine the default units
            Unit defaultUnit = tryParseUnit(defaultUnitString, Unit.class);
            if (defaultUnit == null) {
                throw new InputErrorException("Could not determine default units " + defaultUnitString);
            }

            if (defaultUnit.getClass() != unit.getClass())
                throw new InputErrorException("Cannot convert from %s to %s", defaultUnit.getName(), unit.getName());

            // Determine the conversion factor from units to default units
            conversionFactor = unit.getConversionFactorToUnit(defaultUnit);
        }

        // Parse and convert the value
        return parseDouble(input.get(0), minValue, maxValue, conversionFactor);
    }

    /**
     * Convert the given input to a DoubleVector and apply the given conversion factor
     */
    // TODO static
    public static DoubleVector parseDoubleVector(List<String> input, double minValue, double maxValue, double factor) throws InputErrorException {
        DoubleVector temp = new DoubleVector(input.size());

        for (int i = 0; i < input.size(); i++) {
            try {
                double element = parseDouble(input.get(i), minValue, maxValue, factor);
                temp.add(element);
            } catch (InputErrorException e) {
                throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
            }
        }
        return temp;
    }

    /**
     * Convert the given input to a DoubleVector and apply the given conversion factor
     */
    // TODO check static
    public DoubleVector parseDoubles(KeywordIndex kw, double minValue, double maxValue, Class<? extends Unit> unitType) throws InputErrorException {

        if (unitType == UserSpecifiedUnit.class)
            throw new InputErrorException(INP_ERR_UNITUNSPECIFIED);

        double factor = 1.0d;
        int numArgs = kw.numArgs();
        int numDoubles = numArgs;
        boolean includeIndex = true;

        // Parse the unit portion of the input
        Unit unit = tryParseUnit(kw.getArg(numArgs - 1), unitType);


        // A unit is mandatory except for dimensionless values and time values in RFC8601 date/time format
        if (unit == null && unitType != DimensionlessUnit.class && unitType != TimeUnit.class)
            throw new InputErrorException(INP_ERR_NOUNITFOUND, kw.getArg(numArgs - 1), unitType.getSimpleName());

        if (unit != null) {
            factor = unit.getConversionFactorToSI();
            numDoubles = numArgs - 1;
        }

        // Parse the numeric portion of the input
        DoubleVector temp = new DoubleVector(numDoubles);
        for (int i = 0; i < numDoubles; i++) {
            try {
                // Time input
                if (unitType == TimeUnit.class) {

                    // RFC8601 date/time format
                    if (isRFC8601DateTime(kw.getArg(i))) {
                        double element = parseRFC8601DateTime(kw.getArg(i)) / 1e6;
                        if (element < minValue || element > maxValue)
                            throw new InputErrorException(INP_ERR_DOUBLERANGE, minValue, maxValue, temp);
                        temp.add(element);
                    }
                    // Normal format
                    else {
                        if (unit == null) {
                            includeIndex = false;
                            throw new InputErrorException(INP_ERR_NOUNITFOUND, kw.getArg(numArgs - 1), unitType.getSimpleName());
                        }
                        double element = parseDouble(kw.getArg(i), minValue, maxValue, factor);
                        temp.add(element);
                    }
                }
                // Non-time input
                else {
                    double element = parseDouble(kw.getArg(i), minValue, maxValue, factor);
                    temp.add(element);
                }
            } catch (InputErrorException e) {
                if (includeIndex && numDoubles > 1)
                    throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
                else
                    throw e;
            }
        }
        return temp;
    }

    /**
     * Convert the given input to a DoubleVector and apply the given conversion factor
     */
    // TODO check static
    public DoubleVector parseDoubles(List<String> input, double minValue, double maxValue, Class<? extends Unit> unitType) throws InputErrorException {

        if (unitType == UserSpecifiedUnit.class)
            throw new InputErrorException(INP_ERR_UNITUNSPECIFIED);

        double factor = 1.0d;
        int numDoubles = input.size();

        // Parse the unit portion of the input
        Unit unit = tryParseUnit(input.get(numDoubles - 1), unitType);

        // A unit is mandatory except for dimensionless values and time values in RFC8601 date/time format
        if (unit == null && unitType != DimensionlessUnit.class && unitType != TimeUnit.class)
            throw new InputErrorException(INP_ERR_NOUNITFOUND, input.get(numDoubles - 1), unitType.getSimpleName());

        if (unit != null) {
            factor = unit.getConversionFactorToSI();
            numDoubles = numDoubles - 1;
        }

        // Parse the numeric portion of the input
        DoubleVector temp = new DoubleVector(numDoubles);
        for (int i = 0; i < numDoubles; i++) {
            try {
                // Time input
                if (unitType == TimeUnit.class) {

                    // RFC8601 date/time format
                    if (isRFC8601DateTime(input.get(i))) {
                        double element = parseRFC8601DateTime(input.get(i)) / 1e6;
                        if (element < minValue || element > maxValue)
                            throw new InputErrorException(INP_ERR_DOUBLERANGE, minValue, maxValue, temp);
                        temp.add(element);
                    }
                    // Normal format
                    else {
                        if (unit == null)
                            throw new InputErrorException(INP_ERR_NOUNITFOUND, input.get(numDoubles - 1), unitType.getSimpleName());
                        double element = parseDouble(input.get(i), minValue, maxValue, factor);
                        temp.add(element);
                    }
                }
                // Non-time input
                else {
                    double element = parseDouble(input.get(i), minValue, maxValue, factor);
                    temp.add(element);
                }
            } catch (InputErrorException e) {
                if (numDoubles == 1)
                    throw e;
                else
                    throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
            }
        }
        return temp;
    }

    /**
     * Convert the given input to a DoubleVector including a unit conversion, if necessary
     */
    // TODO check static
    public DoubleVector parseDoubleVector(List<String> data, double minValue, double maxValue, String defaultUnitString) throws InputErrorException {
        // If there is more than one value, and the last one is not a number, then assume it is a unit
        String unitString = data.get(data.size() - 1);
        if (data.size() > 1 && !isDouble(unitString)) {

            // Determine the units
            Unit unit = parseUnit(unitString);

            // Determine the default units
            Unit defaultUnit = tryParseUnit(defaultUnitString, Unit.class);
            if (defaultUnit == null) {
                throw new InputErrorException("Could not determine default units " + defaultUnitString);
            }

            if (defaultUnit.getClass() != unit.getClass())
                throw new InputErrorException("Cannot convert from %s to %s", defaultUnit.getName(), unit.getName());

            // Determine the conversion factor to the default units
            double conversionFactor = unit.getConversionFactorToUnit(defaultUnit);

            // grab all but the final argument (the unit)
            ArrayList<String> numericData = new ArrayList<>(data.size() - 1);
            for (int i = 0; i < data.size() - 1; i++)
                numericData.add(data.get(i));

            return parseDoubleVector(numericData, minValue, maxValue, conversionFactor);
        } else {
            if (defaultUnitString.length() > 0)
                LOG.warn("Missing units.  Assuming %s.", defaultUnitString);
        }

        // Parse and convert the values
        return parseDoubleVector(data, minValue, maxValue, 1.0d);
    }

    // TODO static
    public static String parseString(String input, ArrayList<String> validList) throws InputErrorException {
        return parseString(input, validList, false);
    }

    // TODO static
    public static String parseString(String input, ArrayList<String> validList, boolean caseSensitive)
            throws InputErrorException {
        for (String valid : validList) {
            if (caseSensitive && valid.equals(input))
                return valid;

            if (!caseSensitive && valid.equalsIgnoreCase(input))
                return valid;
        }

        throw new InputErrorException(INP_ERR_BADCHOICE, validList.toString(), input);
    }

    // TODO static
    public static ArrayList<String> parseStrings(KeywordIndex kw, ArrayList<String> validList, boolean caseSensitive) throws InputErrorException {
        ArrayList<String> temp = new ArrayList<>(kw.numArgs());

        for (int i = 0; i < kw.numArgs(); i++) {
            try {
                String element = parseString(kw.getArg(i), validList);
                temp.add(element);
            } catch (InputErrorException e) {
                throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
            }
        }
        return temp;
    }

    // TODO static
    public static <T extends Enum<T>> T parseEnum(Class<T> type, String enumName) {
        try {
            return Enum.valueOf(type, enumName);
        } catch (IllegalArgumentException e) {
            throw new InputErrorException(INP_ERR_BADCHOICE, Arrays.toString(type.getEnumConstants()), enumName);
        } catch (NullPointerException e) {
            throw new InputErrorException(INP_ERR_BADCHOICE, Arrays.toString(type.getEnumConstants()), enumName);
        }
    }

    // TODO check static
    // TODO moving responsibility to event manager
    public Class<? extends Entity> parseEntityType(String entityTypeName) throws InputErrorException {

        if (entityTypeName.equalsIgnoreCase("ObjectType")) {
            return ObjectType.class;
        }

        ObjectType type = tryParseEntity(entityTypeName, ObjectType.class);
        if (type == null)
            throw new InputErrorException("Entity type not found: %s", entityTypeName);

        Class<? extends Entity> klass = type.getJavaClass();
        if (klass == null)
            throw new InputErrorException("ObjectType %s does not have a java class set", entityTypeName);

        return klass;
    }

    // TODO move responsibility or make more generic, wider
    // TODO static
    public static void assertEquals(Class<? extends Unit> u1, Class<? extends Unit> u2) throws InputErrorException {
        if (u1 != u2)
            throw new InputErrorException(INP_ERR_UNITS);
    }

    // TODO static
    public static <T extends Entity> Class<? extends T> checkCast(Class<? extends Entity> klass, Class<T> parent) {
        try {
            return klass.asSubclass(parent);
        } catch (ClassCastException e) {
            throw new InputErrorException(INP_ERR_NOTSUBCLASS, parent.getName(), klass.getName());
        }
    }

    // TODO static
    public static <T> T castImplements(Entity entity, Class<T> type) throws InputErrorException {
        try {
            return type.cast(entity);
        } catch (ClassCastException e) {
            throw new InputErrorException(INP_ERR_INTERFACE, type.getName(), entity.getName());
        }
    }

    // TODO static
    private static <T extends Entity> T castEntity(Entity entity, Class<T> type) throws InputErrorException {
        try {
            return type.cast(entity);
        } catch (ClassCastException e) {
            return null;
        }
    }

    public <T extends Entity> T parseEntity(String choice, Class<T> prototype) throws InputErrorException {
        Entity namedEntity = entityManager.getNamedEntity(choice);
        if (namedEntity == null) {
            throw new InputErrorException(INP_ERR_ENTNAME, choice);
        }
        T castEntity = castEntity(namedEntity, prototype);
        if (castEntity == null) {
            throw new InputErrorException(INP_ERR_ENTCLASS, prototype.getSimpleName(), choice, namedEntity.getClass().getSimpleName());
        }
        return castEntity;
    }

    // TODO moving responsibility to event manager
    public <T extends Entity> T tryParseEntity(String entityName, Class<T> entityType) {
        return castEntity(entityManager.getNamedEntity(entityName), entityType);
    }

    public <T extends Unit> T tryParseUnit(String unitName, Class<T> unitType) {
        return castEntity(entityManager.getNamedEntity(unitName), unitType);
    }

    public Unit parseUnit(String unitName) throws InputErrorException {
        Unit unit = tryParseUnit(unitName, Unit.class);
        if (unit == null)
            throw new InputErrorException("Could not find a unit named: %s", unitName);

        return unit;
    }

    public <T extends Entity> ArrayList<T> parseEntityList(KeywordIndex kw, Class<T> prototype, boolean unique) throws InputErrorException {
        ArrayList<T> entities = new ArrayList<>(kw.numArgs());

        for (int i = 0; i < kw.numArgs(); i++) {
            Entity namedEntity = entityManager.getNamedEntity(kw.getArg(i));
            if (namedEntity == null) {
                throw new InputErrorException(INP_ERR_ENTNAME, kw.getArg(i));
            }

            // If we found a group, expand the list of Entities
            if (namedEntity instanceof Group) {
                ArrayList<Entity> gList = ((Group) namedEntity).getList();
                for (int j = 0; j < gList.size(); j++) {
                    T t = castEntity(gList.get(j), prototype);
                    if (t == null) {
                        throw new InputErrorException(INP_ERR_ENTCLASS, prototype.getSimpleName(), gList.get(j), gList.get(j).getClass().getSimpleName());
                    }
                    entities.add(t);
                }
            } else {
                T castEntity = castEntity(namedEntity, prototype);
                if (castEntity == null) {
                    throw new InputErrorException(INP_ERR_ENTCLASS, prototype.getSimpleName(), kw.getArg(i), namedEntity.getClass().getSimpleName());
                }
                entities.add(castEntity);
            }
        }

        if (unique)
            assertUnique(entities);

        return entities;
    }

    public <T extends Entity> ArrayList<T> parseEntityList(List<String> input, Class<T> prototype, boolean unique) throws InputErrorException {
        ArrayList<T> entities = new ArrayList<>(input.size());

        for (int i = 0; i < input.size(); i++) {
            Entity namedEntity = entityManager.getNamedEntity(input.get(i));
            if (namedEntity == null) {
                throw new InputErrorException(INP_ERR_ENTNAME, input.get(i));
            }

            // If we found a group, expand the list of Entities
            if (namedEntity instanceof Group) {
                ArrayList<Entity> gList = ((Group) namedEntity).getList();
                for (int j = 0; j < gList.size(); j++) {
                    T castEntity = castEntity(gList.get(j), prototype);
                    if (castEntity == null) {
                        throw new InputErrorException(INP_ERR_ENTCLASS, prototype.getSimpleName(), gList.get(j), gList.get(j).getClass().getSimpleName());
                    }
                    entities.add(castEntity);
                }
            } else {
                T castEntity = castEntity(namedEntity, prototype);
                if (castEntity == null) {
                    throw new InputErrorException(INP_ERR_ENTCLASS, prototype.getSimpleName(), input.get(i), namedEntity.getClass().getSimpleName());
                }
                entities.add(castEntity);
            }
        }

        if (unique)
            Input.assertUnique(entities);

        return entities;
    }

    public <T extends Entity> ArrayList<ArrayList<T>> parseListOfEntityLists(KeywordIndex keywordIndex, Class<T> prototype, boolean unique) throws InputErrorException {
        ArrayList<KeywordIndex> subArgs = keywordIndex.getSubArgs();
        ArrayList<ArrayList<T>> entities = new ArrayList<>(subArgs.size());

        for (int i = 0; i < subArgs.size(); i++) {
            try {
                ArrayList<T> element = parseEntityList(subArgs.get(i), prototype, unique);
                entities.add(element);
            } catch (InputErrorException e) {
                throw new InputErrorException(INP_ERR_ELEMENT, i, e.getMessage());
            }
        }
        return entities;
    }

    public <T> ArrayList<T> parseInterfaceEntityList(KeywordIndex keywordIndex, Class<T> prototype, boolean unique) throws InputErrorException {
        ArrayList<T> entities = new ArrayList<>(keywordIndex.numArgs());

        for (int i = 0; i < keywordIndex.numArgs(); i++) {
            Entity namedEntity = entityManager.getNamedEntity(keywordIndex.getArg(i));
            if (namedEntity == null) {
                throw new InputErrorException(INP_ERR_ENTNAME, keywordIndex.getArg(i));
            }

            // If we found a group, expand the list of Entities
            if (namedEntity instanceof Group) {
                ArrayList<Entity> groupedEntities = ((Group) namedEntity).getList();
                for (int j = 0; j < groupedEntities.size(); j++) {
                    T castImplements = castImplements(groupedEntities.get(j), prototype);
                    if (castImplements == null) {
                        throw new InputErrorException(INP_ERR_ENTCLASS, prototype.getSimpleName(), groupedEntities.get(j), groupedEntities.get(j).getClass().getSimpleName());
                    }
                    entities.add(castImplements);
                }
            } else {
                T castImplements = castImplements(namedEntity, prototype);
                if (castImplements == null) {
                    throw new InputErrorException(INP_ERR_ENTCLASS, prototype.getSimpleName(), keywordIndex.getArg(i), namedEntity.getClass().getSimpleName());
                }
                entities.add(castImplements);
            }
        }

        if (unique)
            assertUniqueInterface(entities);

        return entities;
    }

    public Color4d parseColour(KeywordIndex kw) {

        assertCount(kw, 1, 3);

        // Color names
        if (kw.numArgs() == 1) {
            Color4d colorWithName = ColourInput.getColorWithName(kw.getArg(0).toLowerCase());
            if (colorWithName == null) {
                throw new InputErrorException("Color " + kw.getArg(0) + " not found");
            } else {
                return colorWithName;
            }
        }

        // RGB
        else {
            DoubleVector doubleVector = parseDoubles(kw, 0.0d, 255.0d, DimensionlessUnit.class);
            double r = doubleVector.get(0);
            double g = doubleVector.get(1);
            double b = doubleVector.get(2);

            if (r > 1.0f || g > 1.0f || b > 1.0f) {
                r /= 255.0d;
                g /= 255.0d;
                b /= 255.0d;
            }

            return new Color4d(r, g, b);
        }
    }

    public SampleProvider parseSampleExp(KeywordIndex keywordIndex, Entity entity, double minValue, double maxValue, Class<? extends Unit> unitType) {

        assertCount(keywordIndex, 1, 2);

        // If there are two inputs, it could be a number and its unit or an entity and its output
        if (keywordIndex.numArgs() == 2) {

            // A) Try an entity and its output
            try {
                return parseSampleOutput(keywordIndex, unitType);
            } catch (InputErrorException e) {
            }

            // B) Try a number and its unit
            if (unitType == DimensionlessUnit.class)
                throw new InputErrorException(INP_ERR_COUNT, 1, keywordIndex.argString());
            DoubleVector doubleVector = null;
            doubleVector = parseDoubles(keywordIndex, minValue, maxValue, unitType);
            return new SampleConstant(unitType, doubleVector.get(0));
        }

        // If there is only one input, it could be a SampleProvider, a dimensionless constant, or an expression

        // 1) Try parsing a SampleProvider
        SampleProvider sampleProvider = null;
        try {
            Entity parsedEntity = parseEntity(keywordIndex.getArg(0), Entity.class);
            sampleProvider = castImplements(parsedEntity, SampleProvider.class);
        } catch (InputErrorException e) {
        }

        if (sampleProvider != null) {
            if (sampleProvider.getUnitType() != UserSpecifiedUnit.class)
                assertEquals(unitType, sampleProvider.getUnitType());
            return sampleProvider;
        }

        // 2) Try parsing a constant value
        DoubleVector doubleVector = null;
        try {
            doubleVector = parseDoubles(keywordIndex, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, DimensionlessUnit.class);
        } catch (InputErrorException e) {
        }

        if (doubleVector != null) {
            if (unitType != DimensionlessUnit.class)
                throw new InputErrorException(INP_ERR_UNITNOTFOUND, unitType.getSimpleName());
            if (doubleVector.get(0) < minValue || doubleVector.get(0) > maxValue)
                throw new InputErrorException(INP_ERR_DOUBLERANGE, minValue, maxValue, doubleVector.get(0));
            return new SampleConstant(unitType, doubleVector.get(0));
        }

        // 3) Try parsing an expression
        try {
            Expression expression = ExpParser.parseExpression(new ExpEvaluator(entityManager).getParseContext(), keywordIndex.getArg(0));
            new ExpressionValidator(entityManager).validateExpression(expression, entity, unitType);
            return new SampleExpression(expression, entity, unitType, entityManager);
        } catch (ExpressionError e) {
            throw new InputErrorException(e.toString());
        }
    }

    public SampleOutput parseSampleOutput(KeywordIndex keywordIndex, Class<? extends Unit> unitType) {

        Input.assertCount(keywordIndex, 2);
        try {
            Entity parseEntity = parseEntity(keywordIndex.getArg(0), Entity.class);
            if (parseEntity instanceof ObjectType)
                throw new InputErrorException("%s is the name of a class, not an instance", parseEntity.getName());

            String outputName = keywordIndex.getArg(1);
            if (!parseEntity.hasOutput(outputName))
                throw new InputErrorException("Output named %s not found for Entity %s", outputName, parseEntity.getName());

            OutputHandle out = parseEntity.getOutputHandle(outputName);
            if (!out.isNumericValue())
                throw new InputErrorException("Output named %s for Entity %s is not a numeric value.", outputName, parseEntity.getName());

            if (unitType != UserSpecifiedUnit.class && out.getUnitType() != unitType)
                throw new InputErrorException("Output named %s for Entity %s has unit type of %s. Expected %s", outputName, parseEntity.getName(), out.getUnitType(), unitType);

            return new SampleOutput(out);
        } catch (InputErrorException e) {
            throw new InputErrorException(e.getMessage());
        }
    }

    // TODO static
    private static void assertUnique(ArrayList<? extends Entity> list) {
        for (int i = 0; i < list.size(); i++) {
            Entity ent = list.get(i);
            for (int j = i + 1; j < list.size(); j++) {
                if (ent == list.get(j)) {
                    throw new InputErrorException(INP_ERR_NOTUNIQUE, ent.getName());
                }
            }
        }
    }

    // TODO static
    private static void assertUniqueInterface(ArrayList<?> list) {
        for (int i = 0; i < list.size(); i++) {
            Entity entity = (Entity) list.get(i);
            for (int j = i + 1; j < list.size(); j++) {
                if (entity == list.get(j)) {
                    throw new InputErrorException(INP_ERR_NOTUNIQUE, entity.getName());
                }
            }
        }
    }

    // TODO static
    public static void validateIndexedLists(ListInput<?> keys, ListInput<?> values) throws InputErrorException {
        // If no values set, no validation to be done
        if (values.getValue() == null)
            return;

        // values are set but indexed list has not
        if (keys.getValue() == null)
            throw new InputErrorException(INP_VAL_LISTSET, keys.getKeyword(), values.getKeyword());

        // Both are set, but of differing size
        if (keys.getListSize() != values.getListSize())
            throw new InputErrorException(INP_VAL_LISTSIZE, keys.getKeyword(), values.getKeyword());
    }

    // TODO static
    public static void validateInputSize(ListInput<?> list1, ListInput<?> list2) throws InputErrorException {
        // One list is set but not the other
        if (list1.getValue() != null && list2.getValue() == null)
            throw new InputErrorException(INP_VAL_LISTSIZE, list1.getKeyword(), list2.getKeyword());

        if (list1.getValue() == null && list2.getValue() != null)
            throw new InputErrorException(INP_VAL_LISTSIZE, list1.getKeyword(), list2.getKeyword());

        // Both are set, but of differing size
        if (list1.getListSize() != list2.getListSize())
            throw new InputErrorException(INP_VAL_LISTSIZE, list1.getKeyword(), list2.getKeyword());
    }

    // TODO static
    public static void validateIndexedLists(ArrayList<?> keys, DoubleVector values, String keyName, String valueName) throws InputErrorException {
        // If no values set, no validation to be done
        if (values == null)
            return;

        // values are set but indexed list has not
        if (keys == null)
            throw new InputErrorException(INP_VAL_LISTSET, valueName, keyName);

        // Both are set, but of differing size
        if (keys.size() != values.size())
            throw new InputErrorException(INP_VAL_LISTSIZE, keyName, valueName);
    }

    public void reset() {
        this.setDefaultValue(this.getDefaultValue());
        valueTokens = null;
        edited = false;
        isDefault = true;
    }

    /**
     * Assigns the internal state for this input to the same values as the
     * specified input.
     *
     * @param in - input object to be copied.
     */
    public void copyFrom(Input<?> in) {

        @SuppressWarnings("unchecked")
        Input<T> inp = (Input<T>) in;

        // Copy the internal state
        value = inp.value;
        valueTokens = inp.valueTokens;
        isDefault = false;
        edited = true;
    }

    @Override
    public String toString() {
        return String.format("%s", value);
    }

    public final String getKeyword() {
        return keyword;
    }

    public void setKeyword(String str) {
        keyword = str;
    }

    public final String getCategory() {
        return category;
    }

    public boolean isSynonym() {
        return false;
    }

    public String getDefaultText() {
        return defaultText;
    }

    public void setDefaultText(String str) {
        defaultText = str;
    }

    public T getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(T val) {
        defaultValue = val;
        value = val;
    }

    public String getDefaultString() {
        if (defaultValue == null)
            return "";
        return defaultValue.toString();
    }

    public T getValue() {
        return value;
    }

    public boolean getHidden() {
        return hidden;
    }

    public void setHidden(boolean hide) {
        hidden = hide;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean bool) {
        edited = bool;
    }

    public boolean isPromptRequired() {
        return promptRequired;
    }

    public void setPromptRequired(boolean bool) {
        promptRequired = bool;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean bool) {
        required = bool;
    }

    public void validate() throws InputErrorException {
        if (required && isDefault && !hidden)
            throw new InputErrorException("An input must be provided for the keyword '%s'.", keyword);
    }

    public void setTokens(KeywordIndex keywordIndex) {
        isDefault = false;
        if (keywordIndex.numArgs() > 1000) {
            valueTokens = null;
            return;
        }

        valueTokens = keywordIndex.getArgArray();
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void getValueTokens(ArrayList<String> toks) {
        if (valueTokens == null)
            return;

        for (String each : valueTokens)
            toks.add(each);
    }

    public final String getValueString() {
        if (isDefault()) return "";

        ArrayList<String> tmp = new ArrayList<>();
        getValueTokens(tmp);
        if (tmp.size() == 0) return "";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tmp.size(); i++) {
            String dat = tmp.get(i);
            if (dat == null) continue;
            if (i > 0)
                sb.append("  ");

            if (Parser.needsQuoting(dat) && !dat.equals("{") && !dat.equals("}"))
                sb.append("'").append(dat).append("'");
            else
                sb.append(dat);
        }
        return sb.toString();
    }

    public abstract void parse(KeywordIndex kw) throws InputErrorException;

    /**
     * Returns a list of valid options if the input has limited number of
     * choices (e.g TRUE or FALSE for BooleanInput).
     * <p/>
     * This method must be overridden for an input to be shown with a drop-down
     * menu in the Input Editor.
     */
    public ArrayList<String> getValidOptions() {
        return null;
    }

    public String getDefaultStringForKeyInputs(Class<? extends Unit> unitType, String unitString) {

        if (defaultValue == null)
            return "";

        if (defaultValue.getClass() == Boolean.class) {
            if ((Boolean) defaultValue)
                return "TRUE";

            return "FALSE";
        }

        StringBuilder tmp = new StringBuilder();
        if (defaultValue.getClass() == Double.class ||
                defaultValue.getClass() == Integer.class ||
                Entity.class.isAssignableFrom(Double.class)) {
            if (defaultValue.equals(Integer.MAX_VALUE) || defaultValue.equals(Double.POSITIVE_INFINITY))
                return POSITIVE_INFINITY;

            if (defaultValue.equals(Integer.MIN_VALUE) || defaultValue.equals(Double.NEGATIVE_INFINITY))
                return NEGATIVE_INFINITY;

            tmp.append(defaultValue);
        } else if (defaultValue.getClass() == SampleConstant.class ||
                defaultValue.getClass() == TimeSeriesConstantDouble.class) {
            return defaultValue.toString();
        } else if (defaultValue.getClass() == DoubleVector.class) {
            DoubleVector def = (DoubleVector) defaultValue;
            if (def.size() == 0)
                return "";

            tmp.append(def.get(0));
            for (int i = 1; i < def.size(); i++) {
                tmp.append(SEPARATOR);
                tmp.append(def.get(i));
            }
        } else if (defaultValue.getClass() == IntegerVector.class) {
            IntegerVector def = (IntegerVector) defaultValue;
            if (def.size() == 0)
                return "";

            tmp.append(def.get(0));
            for (int i = 1; i < def.size(); i++) {
                tmp.append(SEPARATOR);
                tmp.append(def.get(i));
            }
        } else if (Entity.class.isAssignableFrom(defaultValue.getClass())) {
            tmp.append(((Entity) defaultValue).getName());
        } else {
            return "?????";
        }

        if (unitString == null) {
            tmp.append(SEPARATOR);
            tmp.append(Unit.getSIUnit(unitType));
        } else {
            tmp.append(SEPARATOR);
            tmp.append(unitString);
        }
        return tmp.toString();
    }
}
