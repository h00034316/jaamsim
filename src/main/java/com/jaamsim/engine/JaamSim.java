package com.jaamsim.engine;

import com.jaamsim.basicsim.Simulation;
import com.jaamsim.events.EventManager;
import com.jaamsim.events.ProgressListener;
import com.jaamsim.events.ReportListener;
import com.jaamsim.input.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by tristano on 22/08/15.
 */
public class JaamSim implements Runnable {
    private ArrayList<String> configurationFiles;

    private byte[] configuration;

    private String contextPath;

    private static Logger LOG = LogManager.getLogger(JaamSim.class.getSimpleName());

    public JaamSim() {
        configurationFiles = new ArrayList<>();
    }

    public JaamSim(String args[]) {
        this();
        if(args.length > 0) {
            contextPath = args[0];
        }

        init();
    }

    public void addConfigurationFile(String fileName) {
        configurationFiles.add(fileName);
    }

    public void addConfiguration(byte[] configuration) {
        this.configuration = configuration;
    }

    @Autowired
    private ConfigurationManager configurationManager;

    private Simulation simulation;

    public void init() {
        LOG.debug("Got configuration manager bean: " + configurationManager);

        LOG.info("Loading configuration ... ");
        if(configuration != null) {
            configurationManager.loadConfiguration(configuration);
        } else if (configurationFiles.size() == 0) {
            configurationManager.setRecordEdits(true);
            configurationManager.loadDefault();
        } else {
            File configurationFile = new File(contextPath, configurationFiles.get(0));
            LOG.info("Context path configuration file: " + configurationFile);
            File loadFile;
            if (configurationFile.exists()) {
                loadFile = configurationFile.getAbsoluteFile();
                LOG.info("Loading file from context path: " + loadFile);
            } else {
                File user = new File(System.getProperty("user.dir"));
                LOG.info("Using user dir: " + user);
                loadFile = new File(user, configurationFiles.get(0));
                LOG.info("Loading file from user path: " + loadFile);
            }

            if(loadFile.exists()) {
                configurationManager.loadConfiguration(loadFile);
            } else {
                configurationManager.loadConfiguration(configurationFiles.get(0));
            }
        }

        LOG.info("Simulation Environment Loaded");
        simulation = configurationManager.getSimulation();
        LOG.debug("Got simulation: " + simulation);

        start();
    }

    public void start() {
        new Thread(this).start();
    }

    public void run() {
        LOG.info("Starting JAAMSIM");
        simulation.start();
    }

    public boolean isExecuting() {
        try {
            return simulation.isExecuting();
        } catch(Exception e) {
            LOG.error("Exception: " + e.getMessage());
            return false;
        }
    }

    public void addProgressListener(ProgressListener listener) {
        simulation.addProgressListener(listener);
    }

    public void addReportListener(ReportListener listener) {
        simulation.getConfigurationManager().getEventManager().addReportListener(listener);
    }

    public enum SimState {
        /**
         * model was executed, but no configuration performed
         */
        LOADED,
        /**
         * essential model elements created, no configuration performed
         */
        NOT_CONFIGURED,
        /**
         * model has been configured, not started
         */
        CONFIGURED,
        /**
         * model is presently executing events
         */
        RUNNING,
        /**
         * model has run, but presently is paused
         */
        PAUSED
    }
}
