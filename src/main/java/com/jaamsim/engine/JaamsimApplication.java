package com.jaamsim.engine;

import com.jaamsim.basicsim.EntityManager;
import com.jaamsim.events.EventManager;
import com.jaamsim.input.ConfigurationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Created by tristano on 19/12/15.
 */
@SpringBootApplication
public class JaamsimApplication {

    @Bean(initMethod = "init")
    public JaamSim jaamSim() {
        JaamSim jaamSim = new JaamSim();
        jaamSim.addConfigurationFile("/Volumes/DEVHD/projects/dissertation/dev/files/CafeModel_10.cfg");
        return jaamSim;
    }

    @Bean(initMethod = "clear")
    public ConfigurationManager configurationManager() {
        return new ConfigurationManager(entityManager());
    }

    @Bean
    public EventManager eventManager() {
        return new EventManager("DefaultEventManager");
    }

    @Bean
    public EntityManager entityManager() {
        return new EntityManager();
    }

    @Autowired
    private JaamSim jaamSim;

    public static void main(String[] args) {
        SpringApplication.run(JaamsimApplication.class, args);
    }
}
