/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2002-2011 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.basicsim;

import com.jaamsim.input.*;
import com.jaamsim.units.Unit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public class EntityManager {
    private static final Logger LOG = LogManager.getFormatterLogger(EntityManager.class.getSimpleName());

    private static final String ERROR_DEFINE_USED = "The name: %s has already been used and is a %s";

    private final ArrayList<Entity> allInstances;
    private final HashMap<String, Entity> namedEntities;
    private AtomicLong entityCount = new AtomicLong(0);

    private Simulation simulation;

    public EntityManager() {
        allInstances = new ArrayList<>(100);
        namedEntities = new HashMap<>(100);
    }

    public EntityManager(Simulation simulation) {
        this();
        this.simulation = simulation;
    }

    private long getNextID() {
        return entityCount.incrementAndGet();
    }

    public ArrayList<? extends Entity> getAll() {
        synchronized (allInstances) {
            return allInstances;
        }
    }

    public <T extends Entity> ArrayList<T> getInstancesOf(Class<T> proto) {
        ArrayList<T> instanceList = new ArrayList<>();

        for (Entity each : allInstances) {
            if (proto == each.getClass()) {
                instanceList.add(proto.cast(each));
            }
        }

        return instanceList;
    }

    public <T extends Entity> InstanceIterable<T> getInstanceIterator(Class<T> proto) {
        return new InstanceIterable<>(proto, this);
    }

    public <T extends Entity> ClonesOfIterable<T> getClonesOfIterator(Class<T> proto) {
        return new ClonesOfIterable<>(proto, this);
    }

    /**
     * Returns an iterator over the given proto class, but also filters only those
     * objects that implement the given interface class.
     *
     * @return
     */
    public <T extends Entity> ClonesOfIterableInterface<T> getClonesOfIterator(Class<T> proto, Class<?> iface) {
        return new ClonesOfIterableInterface<>(proto, iface, this);
    }

    public <T extends Entity> T fastCopy(T ent, String name) {
        // Create the new entity
        @SuppressWarnings("unchecked")
        T ret = (T) generateEntityWithName(ent.getClass(), name);
        // Loop through the original entity's inputs
        ArrayList<Input<?>> orig = ent.getEditableInputs();
        for (int i = 0; i < orig.size(); i++) {
            Input<?> sourceInput = orig.get(i);

            // Default values do not need to be copied
            if (sourceInput.isDefault() || sourceInput.isSynonym())
                continue;

            // Assign the value to the copied entity's input
            Input<?> targetInput = ret.getEditableInputs().get(i);
            targetInput.copyFrom(sourceInput);
            ret.updateForInput(targetInput);
        }
        return ret;
    }

    public <T extends Entity> ArrayList<T> getClonesOf(Class<T> proto) {
        ArrayList<T> cloneList = new ArrayList<>();

        for (Entity each : allInstances) {
            if (proto.isAssignableFrom(each.getClass())) {
                cloneList.add(proto.cast(each));
            }
        }

        return cloneList;
    }

    public Entity idToEntity(long id) {
        // TODO replace list with map
        synchronized (allInstances) {
            for (Entity entity : allInstances) {
                if (entity.getEntityNumber() == id) {
                    return entity;
                }
            }
            return null;
        }
    }

    public long getEntitySequence() {
        long sequence = (long) allInstances.size() << 32;
        sequence += entityCount.get();
        return sequence;
    }

    public <T extends Entity> T generateEntityWithName(Class<T> prototype, String name) {

        T entity = null;
        try {
            entity = prototype.newInstance();
            entity.setFlag(Entity.FLAG_GENERATED);
            entity.setEntityNumber(getNextID());
            if (name != null)
                entity.setName(name);
            else
                entity.setName(prototype.getSimpleName() + "-" + entity.getEntityNumber());
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } finally {
            if (entity == null) {
                LOG.error("Could not create new Entity: %s", name);
                return null;
            }
        }
        return entity;
    }

    public <T extends Entity> T defineEntityWithUniqueName(Class<T> prototype, String entityName, String separator, boolean addedEntity) {

        String name = entityName;
        int entityNum = 1;

        while (getNamedEntity(name) != null) {
            name = String.format("%s%s%d", entityName, separator, entityNum++);
        }

        return defineEntity(prototype, name, addedEntity);
    }

    /**
     * if addedEntity is true then this is an entity defined
     * by user interaction or after added record flag is found;
     * otherwise, it is from an input file define statement
     * before the model is configured
     *
     * @param prototype
     * @param entityName
     * @param addedEntity
     */
    public <T extends Entity> T defineEntity(Class<T> prototype, String entityName, boolean addedEntity) {
        Entity existingEntity = castEntity(getNamedEntity(entityName), Entity.class);
        if (existingEntity != null) {
            LOG.error(ERROR_DEFINE_USED, entityName, existingEntity.getClass().getSimpleName());
            return null;
        }

        T entity = null;
        try {
            entity = prototype.newInstance();
            if (addedEntity) {
                entity.setFlag(Entity.FLAG_ADDED);
            }
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } finally {
            if (entity == null) {
                LOG.error("Could not create new Entity: %s", entityName);
                return null;
            }
        }

        setEntityName(entity, entityName);
        return entity;
    }

    private static <T extends Entity> T castEntity(Entity entity, Class<T> type) throws InputErrorException {
        try {
            return type.cast(entity);
        } catch (ClassCastException e) {
            return null;
        }
    }

    public Class<? extends Entity> getEntityType(String entityTypeName) throws InputErrorException {

        if (entityTypeName.equalsIgnoreCase("ObjectType")) {
            return ObjectType.class;
        }

        ObjectType type = getEntity(entityTypeName, ObjectType.class);
        if (type == null)
            throw new InputErrorException("Entity type not found: %s", entityTypeName);

        Class<? extends Entity> klass = type.getJavaClass();
        if (klass == null)
            throw new InputErrorException("ObjectType %s does not have a java class set", entityTypeName);

        return klass;
    }

    public <T extends Entity> T getEntity(String entityName, Class<T> entityType) {
        return castEntity(getNamedEntity(entityName), entityType);
    }

    public void setEntityName(Entity entity, String name) {

        entity.setName(name);
        if(StringUtils.isEmpty(entity.getName())) {
            entity.setFlag(Entity.FLAG_GENERATED);
            entity.setName(name);
            entity.clearFlag(Entity.FLAG_GENERATED);
        }

        namedEntities.put(name, entity);
        allInstances.add(entity);
    }

    /**
     * Assigns a new name to the given entity.
     *
     * @param entity     - entity to be renamed
     * @param newName - new name for the entity
     */
    public void renameEntity(Entity entity, String newName) {

        String oldName = entity.getName();

        // Check that the entity was defined AFTER the RecordEdits command
        if (!entity.testFlag(Entity.FLAG_ADDED))
            throw new ErrorException("Cannot rename an entity that was defined before the RecordEdits command.");

        // Check that the name has not been used already
        Entity existingEntity = castEntity(getNamedEntity(newName), Entity.class);
        if (existingEntity != null && existingEntity != entity)
            throw new ErrorException("Entity name: %s is already in use.", newName);

        // Rename the entity
        entity.setName(newName);
        synchronized (namedEntities) {
            namedEntities.remove(oldName);
            namedEntities.put(newName, entity);
        }
    }

    public Entity getNamedEntity(String name) {
        synchronized(namedEntities) {
            return namedEntities.get(name);
        }
    }

    public void kill(Entity entity) {
        synchronized (allInstances) {
            for (int i = 0; i < allInstances.size(); i++) {
                if(allInstances.get(i) == entity) {
                    allInstances.remove(i);
                    entity.kill();
                }
            }
        }

        if (!entity.testFlag(Entity.FLAG_GENERATED)) {
            synchronized (namedEntities) {
                if (namedEntities.get(entity.getName()) == entity) {
                    namedEntities.remove(entity.getName());
                }
            }
        }
    }

    public void killEntities() {
        synchronized (allInstances) {
            for (Entity entity : allInstances) {
                entity.kill();
            }
            allInstances.clear();
        }

        synchronized (namedEntities) {
            namedEntities.clear();
        }

        simulation = null;
    }

    public <T extends Unit> T getUnit(String unitName, Class<T> unitType) {
        return castEntity(getNamedEntity(unitName), unitType);
    }

    public void setSimulation(Simulation simulation) {
        this.simulation = simulation;
    }

    public Simulation getSimulation() {
        if(simulation == null) {
            for (Entity entity : getAll()) {
                if (entity instanceof Simulation) {
                    simulation = (Simulation) entity;
                    simulation.setEntityManager(this);
                    break;
                }
            }
        }
        return simulation;
    }
}
