/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2014 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.basicsim;

import com.jaamsim.events.EventManager;
import com.jaamsim.events.ProcessTarget;
import com.jaamsim.input.ConfigurationManager;

public class InitModelTarget extends ProcessTarget {

    @Override
    public String getDescription() {
        return "SimulationInit";
    }

    @Override
    public void process() {

        // Initialise each entity
        for (int i = 0; i < getConfigurationManager().getEntityManager().getAll().size(); i++) {
            getConfigurationManager().getEntityManager().getAll().get(i).earlyInit();
        }

        // Initialise each entity a second time
        for (int i = 0; i < getConfigurationManager().getEntityManager().getAll().size(); i++) {
            getConfigurationManager().getEntityManager().getAll().get(i).lateInit();
        }

        // Start each entity
        double startTime = getConfigurationManager().getSimulation().getStartTime();
        for (int i = getConfigurationManager().getEntityManager().getAll().size() - 1; i >= 0; i--) {
            getConfigurationManager().getEventManager().scheduleSeconds(startTime, 0, false, new StartUpTarget(getConfigurationManager().getEntityManager().getAll().get(i)), null);
        }

        // Schedule the initialisation period
        if (getConfigurationManager().getSimulation().getPrintReport()) {   //TODO remove once TLS reports have been integrated with JaamSim
            if (getConfigurationManager().getSimulation().getInitializationTime() > 0.0) {
                double clearTime = startTime + getConfigurationManager().getSimulation().getInitializationTime();
                getConfigurationManager().getEventManager().scheduleSeconds(clearTime, 5, false, new ClearStatisticsTarget(), null);
            }
        }

        // Schedule the end of the simulation run
        double endTime = getConfigurationManager().getSimulation().getEndTime();
        EndModelTarget endModelTarget = new EndModelTarget();
        endModelTarget.setConfigurationManager(getConfigurationManager());
        getConfigurationManager().getEventManager().scheduleSeconds(endTime, 5, false, endModelTarget, null);
    }
}
