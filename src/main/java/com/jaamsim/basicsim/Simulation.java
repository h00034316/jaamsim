/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2002-2011 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.basicsim;

import com.jaamsim.events.EventTimeListener;
import com.jaamsim.events.ProgressListener;
import com.jaamsim.input.*;
import com.jaamsim.math.Vec3d;
// import com.jaamsim.ui.*;
import com.jaamsim.units.DistanceUnit;
import com.jaamsim.units.TimeUnit;
import com.jaamsim.units.Unit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Simulation provides the basic structure for the Entity model lifetime of earlyInit,
 * startUp and doEndAt.  The initial processtargets required to start the model are
 * added to the eventmanager here.  This class also acts as a bridge to the UI by
 * providing controls for the various windows.
 */
public class Simulation extends Entity implements EventTimeListener {
    private static final Logger LOG = LogManager.getFormatterLogger(Simulation.class.getSimpleName());

    public static final int DEFAULT_REAL_TIME_FACTOR = 1;
    public static final int MIN_REAL_TIME_FACTOR = 1;
    public static final int MAX_REAL_TIME_FACTOR = 1000000;

    // Key Inputs tab
    @Keyword(description = "The duration of the simulation run in which all statistics will be recorded.", example = "Simulation Duration { 8760 h }")
    private final ValueInput runDuration;

    @Keyword(description = "The initialization interval for the simulation run. The model will run "
            + "for the InitializationDuration interval and then clear the statistics and execute for the "
            + "specified RunDuration interval. The total length of the simulation run will be the sum of "
            + "the InitializationDuration and RunDuration inputs.",
            example = "Simulation Initialization { 720 h }")
    private final ValueInput initializationTime;

    @Keyword(description = "Indicates whether an output report will be printed at the end of the simulation run.",
            example = "Simulation PrintReport { TRUE }")
    private final BooleanInput printReport;

    @Keyword(description = "The directory in which to place the output report. Defaults to the "
            + "directory containing the configuration file for the run.",
            example = "Simulation ReportDirectory { 'c:\reports\' }")
    private final DirInput reportDirectory;

    @Keyword(description = "The length of time represented by one simulation tick.",
            example = "Simulation TickLength { 1e-6 s }")
    private final ValueInput tickLengthInput;

    @Keyword(description = "Indicates whether to close the program on completion of the simulation run.", example = "Simulation ExitAtStop { TRUE }")
    private final BooleanInput exitAtStop;

    @Keyword(description = "Global seed that sets the substream for each probability distribution. "
            + "Must be an integer >= 0. GlobalSubstreamSeed works together with each probability "
            + "distribution's RandomSeed keyword to determine its random sequence. It allows the "
            + "user to change all the random sequences in a model with a single input.",
            example = "Simulation GlobalSubstreamSeed { 5 }")
    private final IntegerInput globalSeedInput;

    // GUI tab
    @Keyword(description = "An optional list of units to be used for displaying model outputs.",
            example = "Simulation DisplayedUnits { h kt }")
    private final EntityListInput<? extends Unit> displayedUnits;

    @Keyword(description = "If TRUE, a dragged object will be positioned to the nearest grid point.",
            example = "Simulation SnapToGrid { TRUE }")
    private final BooleanInput snapToGrid;

    @Keyword(description = "The distance between snap grid points.",
            example = "Simulation SnapGridSpacing { 1 m }")
    private final ValueInput snapGridSpacing;

    @Keyword(description = "The distance moved by the selected entity when the an arrow key is pressed.",
            example = "Simulation IncrementSize { 1 cm }")
    private final ValueInput incrementSize;

    @Keyword(description = "A Boolean to turn on or off real time in the simulation run",
            example = "Simulation RealTime { TRUE }")
    private final BooleanInput realTime;

    @Keyword(description = "The real time speed up factor",
            example = "Simulation RealTimeFactor { 1200 }")
    private final IntegerInput realTimeFactor;

    @Keyword(description = "The time at which the simulation will be paused.",
            example = "Simulation PauseTime { 200 h }")
    private final ValueInput pauseTime;

    @Keyword(description = "Indicates whether the Model Builder tool should be shown on startup.",
            example = "Simulation ShowModelBuilder { TRUE }")
    private final BooleanInput showModelBuilder;

    @Keyword(description = "Indicates whether the Object Selector tool should be shown on startup.",
            example = "Simulation ShowObjectSelector { TRUE }")
    private final BooleanInput showObjectSelector;

    @Keyword(description = "Indicates whether the Input Editor tool should be shown on startup.",
            example = "Simulation ShowInputEditor { TRUE }")
    private final BooleanInput showInputEditor;

    @Keyword(description = "Indicates whether the Output Viewer tool should be shown on startup.",
            example = "Simulation ShowOutputViewer { TRUE }")
    private final BooleanInput showOutputViewer;

    @Keyword(description = "Indicates whether the Output Viewer tool should be shown on startup.",
            example = "Simulation ShowPropertyViewer { TRUE }")
    private final BooleanInput showPropertyViewer;

    @Keyword(description = "Indicates whether the Log Viewer tool should be shown on startup.",
            example = "Simulation ShowLogViewer { TRUE }")
    private final BooleanInput showLogViewer;

    @Keyword(description = "Time at which the simulation run is started (hh:mm).",
            example = "Simulation StartTime { 2160 h }")
    private final ValueInput startTimeInput;

    // Hidden keywords
    @Keyword(description = "If the value is TRUE, then the input report file will be printed after "
            + "loading the configuration file.  The input report can always be generated when "
            + "needed by selecting \"Print Input Report\" under the File menu.",
            example = "Simulation PrintInputReport { TRUE }")
    private final BooleanInput printInputReport;

    @Keyword(description = "This is placeholder description text",
            example = "This is placeholder example text")
    private final BooleanInput traceEventsInput;

    @Keyword(description = "This is placeholder description text",
            example = "This is placeholder example text")
    private final BooleanInput verifyEventsInput;

    private double timeScale; // the scale from discrete to continuous time
    private double startTime; // simulation time (seconds) for the start of the run (not necessarily zero)
    private double endTime;   // simulation time (seconds) for the end of the run

    private String modelName = "JaamSim";

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    {
        // Key Inputs tab
        runDuration = new ValueInput("RunDuration", "Key Inputs", 31536000.0d);
        runDuration.setUnitType(TimeUnit.class);
        runDuration.setValidRange(1e-15d, Double.POSITIVE_INFINITY);

        initializationTime = new ValueInput("InitializationDuration", "Key Inputs", 0.0);
        initializationTime.setUnitType(TimeUnit.class);
        initializationTime.setValidRange(0.0d, Double.POSITIVE_INFINITY);

        printReport = new BooleanInput("PrintReport", "Key Inputs", false);

        reportDirectory = new DirInput("ReportDirectory", "Key Inputs", null);

        tickLengthInput = new ValueInput("TickLength", "Key Inputs", 1e-6d);
        tickLengthInput.setUnitType(TimeUnit.class);
        tickLengthInput.setValidRange(1e-9d, 5.0d);

        exitAtStop = new BooleanInput("ExitAtStop", "Key Inputs", false);

        globalSeedInput = new IntegerInput("GlobalSubstreamSeed", "Key Inputs", 0);
        globalSeedInput.setValidRange(0, Integer.MAX_VALUE);

        // GUI tab
        displayedUnits = new EntityListInput<>(Unit.class, "DisplayedUnits", "GUI", null);
        displayedUnits.setDefaultText("SI Units");
        displayedUnits.setPromptRequired(false);

        realTime = new BooleanInput("RealTime", "GUI", false);
        realTime.setPromptRequired(false);

        snapToGrid = new BooleanInput("SnapToGrid", "GUI", false);
        snapToGrid.setPromptRequired(false);

        snapGridSpacing = new ValueInput("SnapGridSpacing", "GUI", 0.1d);
        snapGridSpacing.setUnitType(DistanceUnit.class);
        snapGridSpacing.setValidRange(1.0e-6, Double.POSITIVE_INFINITY);
        snapGridSpacing.setPromptRequired(false);

        incrementSize = new ValueInput("IncrementSize", "GUI", 0.1d);
        incrementSize.setUnitType(DistanceUnit.class);
        incrementSize.setValidRange(1.0e-6, Double.POSITIVE_INFINITY);
        incrementSize.setPromptRequired(false);

        realTimeFactor = new IntegerInput("RealTimeFactor", "GUI", DEFAULT_REAL_TIME_FACTOR);
        realTimeFactor.setValidRange(MIN_REAL_TIME_FACTOR, MAX_REAL_TIME_FACTOR);
        realTimeFactor.setPromptRequired(false);

        pauseTime = new ValueInput("PauseTime", "GUI", Double.POSITIVE_INFINITY);
        pauseTime.setUnitType(TimeUnit.class);
        pauseTime.setValidRange(0.0d, Double.POSITIVE_INFINITY);
        pauseTime.setPromptRequired(false);

        showModelBuilder = new BooleanInput("ShowModelBuilder", "GUI", false);
        showModelBuilder.setPromptRequired(false);

        showObjectSelector = new BooleanInput("ShowObjectSelector", "GUI", false);
        showObjectSelector.setPromptRequired(false);

        showInputEditor = new BooleanInput("ShowInputEditor", "GUI", false);
        showInputEditor.setPromptRequired(false);

        showOutputViewer = new BooleanInput("ShowOutputViewer", "GUI", false);
        showOutputViewer.setPromptRequired(false);

        showPropertyViewer = new BooleanInput("ShowPropertyViewer", "GUI", false);
        showPropertyViewer.setPromptRequired(false);

        showLogViewer = new BooleanInput("ShowLogViewer", "GUI", false);
        showLogViewer.setPromptRequired(false);

        // Hidden keywords
        startTimeInput = new ValueInput("StartTime", "Key Inputs", 0.0d);
        startTimeInput.setUnitType(TimeUnit.class);
        startTimeInput.setValidRange(0.0d, Double.POSITIVE_INFINITY);

        traceEventsInput = new BooleanInput("TraceEvents", "Key Inputs", false);
        verifyEventsInput = new BooleanInput("VerifyEvents", "Key Inputs", false);

        printInputReport = new BooleanInput("PrintInputReport", "Key Inputs", false);

        // Initialize basic model information
        startTime = 0.0;
        endTime = 8760.0 * 3600.0;
    }

    {
        // Key Inputs tab
        this.addInput(runDuration);
        this.addInput(initializationTime);
        this.addInput(printReport);
        this.addInput(reportDirectory);
        this.addInput(tickLengthInput);
        this.addInput(exitAtStop);
        this.addInput(globalSeedInput);

        // GUI tab
        this.addInput(displayedUnits);
        this.addInput(snapToGrid);
        this.addInput(snapGridSpacing);
        this.addInput(incrementSize);
        this.addInput(realTime);
        this.addInput(realTimeFactor);
        this.addInput(pauseTime);
        this.addInput(showModelBuilder);
        this.addInput(showObjectSelector);
        this.addInput(showInputEditor);
        this.addInput(showOutputViewer);
        this.addInput(showPropertyViewer);
        this.addInput(showLogViewer);

        // Hidden keywords
        this.addInput(startTimeInput);
        this.addInput(traceEventsInput);
        this.addInput(verifyEventsInput);
        this.addInput(printInputReport);

        // Hide various keywords
        startTimeInput.setHidden(true);
        traceEventsInput.setHidden(true);
        verifyEventsInput.setHidden(true);
        printInputReport.setHidden(true);
    }

    public Simulation() {
    }

    public void clear() {
        initializationTime.reset();
        runDuration.reset();
        pauseTime.reset();
        tickLengthInput.reset();
        traceEventsInput.reset();
        verifyEventsInput.reset();
        printInputReport.reset();
        realTimeFactor.reset();
        realTime.reset();
        updateRealTime();
        exitAtStop.reset();

        startTimeInput.reset();

        showModelBuilder.reset();
        showObjectSelector.reset();
        showInputEditor.reset();
        showOutputViewer.reset();
        showPropertyViewer.reset();
        showLogViewer.reset();

        // Initialize basic model information
        startTime = 0.0;
        endTime = 8760.0 * 3600.0;

        // Kill all entities except simulation
        while (entityManager.getAll().size() > 0) {
            Entity entity = entityManager.getAll().get(entityManager.getAll().size() - 1);
            entityManager.kill(entity);
        }

        progressListeners.clear();
        entityManager = null;
        configurationManager = null;
    }

    /**
     * Initializes and starts the model
     * 1) Initializes EventManager to accept events.
     * 2) calls startModel() to allow the model to add its starting events to EventManager
     * 3) start EventManager processing events
     */
    public void start() {

        getConfigurationManager().getEventManager().setTimeListener(this);

        // Validate each entity based on inputs only
        for (int i = 0; i < entityManager.getAll().size(); i++) {
            try {
                entityManager.getAll().get(i).validate();
            } catch (Throwable e) {
            	LOG.error("%s: Validation error- %s", entityManager.getAll().get(i).getName(), e.getMessage());
                // TODO decouple ui
            	// LogBox.format("%s: Validation error- %s", Entity.getAll().get(i).getName(), e.getMessage());
                // GUIFrame.showErrorDialog("Input Error Detected During Validation",
                //        "%s: %-70s",
                //        Entity.getAll().get(i).getName(), e.getMessage());

                // GUIFrame.instance().updateForSimulationState(GUIFrame.SIM_STATE_CONFIGURED);
                return;
            }
        }

        getConfigurationManager().prepareReportDirectory();
        getConfigurationManager().getEventManager().clear();
        getConfigurationManager().getEventManager().setTraceListener(null);

        if (traceEvents()) {
            String evtName = getConfigurationManager().getConfigurationFile().getParentFile() + File.separator + getConfigurationManager().getRunName() + ".evt";
            EventRecorder rec = new EventRecorder(evtName);
            getConfigurationManager().getEventManager().setTraceListener(rec);
        } else if (verifyEvents()) {
            String evtName = getConfigurationManager().getConfigurationFile().getParentFile() + File.separator + getConfigurationManager().getRunName() + ".evt";
            EventTracer trc = new EventTracer(evtName);
            getConfigurationManager().getEventManager().setTraceListener(trc);
        }

        getConfigurationManager().getEventManager().setTickLength(tickLengthInput.getValue());
        setSimTimeScale(getConfigurationManager().getEventManager().secondsToNearestTick(3600.0d));
        // TODO decouple ui
        // FrameBox.setSecondsPerTick(tickLengthInput.getValue());

        startTime = startTimeInput.getValue();
        endTime = startTime + getInitializationTime() + getRunDuration();

        InitModelTarget initModelTarget = new InitModelTarget();
        initModelTarget.setConfigurationManager(getConfigurationManager());
        getConfigurationManager().getEventManager().scheduleProcessExternal(0, 0, false, initModelTarget, null);
        getConfigurationManager().getEventManager().resume(getConfigurationManager().getEventManager().secondsToNearestTick(getPauseTime()));
    }

    private final List<ProgressListener> progressListeners = new ArrayList<>();

    public final void addProgressListener(ProgressListener listener) {
        if (listener != null) {
            progressListeners.add(listener);
            listener.update(getProgress());
        }
    }

    public void tickUpdate(long tick) {
        updateProgress();
    }

    public void timeRunning(boolean running) {}

    private void updateProgress() {
        int progress = getProgress();
        for(ProgressListener listener : progressListeners) {
            listener.update(progress);
        }
    }

    private int getProgress() {
        double duration = getRunDuration() + getInitializationTime();
        double timeElapsed = getSimulationSeconds() - getStartTime();
        return (int) (timeElapsed * 100.0d / duration);
    }


    public int getSubstreamNumber() {
        return globalSeedInput.getValue();
    }

    public boolean getPrintReport() {
        return printReport.getValue();
    }

    public boolean traceEvents() {
        return traceEventsInput.getValue();
    }

    public boolean verifyEvents() {
        return verifyEventsInput.getValue();
    }

    public void setSimTimeScale(double scale) {
        timeScale = scale;
    }

    public double getSimTimeFactor() {
        return timeScale;
    }

    public double getEventTolerance() {
        return (1.0d / getSimTimeFactor());
    }

    public double getTickLength() {
        return tickLengthInput.getValue();
    }

    public double getPauseTime() {
        return pauseTime.getValue();
    }

    /**
     * Returns the start time of the run.
     *
     * @return - simulation time in seconds for the start of the run.
     */
    public double getStartTime() {
        return startTime;
    }

    /**
     * Returns the end time of the run.
     *
     * @return - simulation time in seconds when the current run will stop.
     */
    public double getEndTime() {
        return endTime;
    }

    /**
     * Returns the duration of the run (not including intialization)
     */
    public double getRunDuration() {
        return runDuration.getValue();
    }

    /**
     * Returns the duration of the initialization period
     */
    public double getInitializationTime() {
        return initializationTime.getValue();
    }

    public double getIncrementSize() {
        return incrementSize.getValue();
    }

    public boolean isSnapToGrid() {
        return snapToGrid.getValue();
    }

    public double getSnapGridSpacing() {
        return snapGridSpacing.getValue();
    }

    /**
     * Returns the nearest point on the snap grid to the given coordinate.
     * To avoid dithering, the new position must be at least one grid space
     * from the old position.
     *
     * @param newPos - new coordinate for the object
     * @param oldPos - present coordinate for the object
     * @return newest snap grid point.
     */
    public Vec3d getSnapGridPosition(Vec3d newPos, Vec3d oldPos) {
        double spacing = snapGridSpacing.getValue();
        Vec3d ret = new Vec3d(newPos);
        if (Math.abs(newPos.x - oldPos.x) < spacing)
            ret.x = oldPos.x;
        if (Math.abs(newPos.y - oldPos.y) < spacing)
            ret.y = oldPos.y;
        if (Math.abs(newPos.z - oldPos.z) < spacing)
            ret.z = oldPos.z;
        return getSnapGridPosition(ret);
    }

    /**
     * Returns the nearest point on the snap grid to the given coordinate.
     *
     * @param pos - position to be adjusted
     * @return nearest snap grid point.
     */
    public Vec3d getSnapGridPosition(Vec3d pos) {
        double spacing = snapGridSpacing.getValue();
        Vec3d ret = new Vec3d(pos);
        ret.x = spacing * Math.rint(ret.x / spacing);
        ret.y = spacing * Math.rint(ret.y / spacing);
        ret.z = spacing * Math.rint(ret.z / spacing);
        return ret;
    }

    void updateRealTime() {
        getConfigurationManager().getEventManager().setExecuteRealTime(realTime.getValue(), realTimeFactor.getValue());
        // TODO to be removed to a controller
        // TODO GUIFrame.instance().updateForRealTime(realTime.getValue(), realTimeFactor.getValue());
    }

    static void updatePauseTime() {
    	// TODO decouple ui
        // GUIFrame.instance().updateForPauseTime(pauseTime.getValueString());
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String newModelName) {
        modelName = newModelName;
    }

    public boolean getExitAtStop() {
        return exitAtStop.getValue();
    }

    public boolean getPrintInputReport() {
        return printInputReport.getValue();
    }

//    public static void setWindowVisible(JFrame f, boolean visible) {
    	// TODO decouple ui
//        f.setVisible(visible);
//        if (visible)
//            f.toFront();
//    }

    /**
     * Re-open any Tools windows that have been closed temporarily.
     */
    public static void showActiveTools() {
//        setWindowVisible(EntityPallet.getInstance(), showModelBuilder.getValue());
//        setWindowVisible(ObjectSelector.getInstance(), showObjectSelector.getValue());
//        setWindowVisible(EditBox.getInstance(), showInputEditor.getValue());
//        setWindowVisible(OutputBox.getInstance(), showOutputViewer.getValue());
//        setWindowVisible(PropertyBox.getInstance(), showPropertyViewer.getValue());
//        setWindowVisible(LogBox.getInstance(), showLogViewer.getValue());
    }

    /**
     * Closes all the Tools windows temporarily.
     */
    public static void closeAllTools() {
//        setWindowVisible(EntityPallet.getInstance(), false);
//        setWindowVisible(ObjectSelector.getInstance(), false);
//        setWindowVisible(EditBox.getInstance(), false);
//        setWindowVisible(OutputBox.getInstance(), false);
//        setWindowVisible(PropertyBox.getInstance(), false);
//        setWindowVisible(LogBox.getInstance(), false);
    }

    @Override
    public void updateForInput(Input<?> input) {
        super.updateForInput(input);

        if (input == realTimeFactor || input == realTime) {
            updateRealTime();
            return;
        }

        if (input == pauseTime) {
            updatePauseTime();
            return;
        }

        if (input == reportDirectory) {
            getConfigurationManager().setReportDirectory(reportDirectory.getDir());
            return;
        }

        if (input == displayedUnits) {
            if (displayedUnits.getValue() == null)
                return;
            for (Unit u : displayedUnits.getValue()) {
                Unit.setPreferredUnit(u.getClass(), u);
            }
            return;
        }

        if (input == showModelBuilder) {
        	// TODO decouple GUI
            // setWindowVisible(EntityPallet.getInstance(), showModelBuilder.getValue());
            return;
        }

        if (input == showObjectSelector) {
        	// TODO decouple GUI
            // setWindowVisible(ObjectSelector.getInstance(), showObjectSelector.getValue());
            return;
        }

        if (input == showInputEditor) {
        	// TODO decouple GUI
            // setWindowVisible(EditBox.getInstance(), showInputEditor.getValue());
            // FrameBox.reSelectEntity();
            return;
        }

        if (input == showOutputViewer) {
        	// TODO decouple GUI
            // setWindowVisible(OutputBox.getInstance(), showOutputViewer.getValue());
            // FrameBox.reSelectEntity();
            return;
        }

        if (input == showPropertyViewer) {
        	// TODO decouple GUI
            // setWindowVisible(PropertyBox.getInstance(), showPropertyViewer.getValue());
            // FrameBox.reSelectEntity();
            return;
        }

        if (input == showLogViewer) {
        	// TODO decouple GUI
            // setWindowVisible(LogBox.getInstance(), showLogViewer.getValue());
            // FrameBox.reSelectEntity();
            return;
        }
    }

    @Output(name = "Configuration File",
            description = "The present configuration file.")
    public String getConfigFileName(double simTime) {
        return getConfigurationManager().getConfigurationFile().getPath();
    }

    public boolean isExecuting() {
        return getConfigurationManager().getEventManager().isProcessRunning();
    }

    public double getSimulationSeconds() {
        return getConfigurationManager().getEventManager()._simSeconds();
    }
}
