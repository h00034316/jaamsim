/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2002-2011 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.basicsim;

import com.jaamsim.events.Conditional;
import com.jaamsim.events.EventHandle;
import com.jaamsim.events.EventManager;
import com.jaamsim.events.ProcessTarget;
import com.jaamsim.input.*;
import com.jaamsim.units.DimensionlessUnit;
import com.jaamsim.units.TimeUnit;
import com.jaamsim.units.Unit;
import com.jaamsim.units.UserSpecifiedUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Abstract class that encapsulates the methods and data needed to create a
 * simulation object. Encapsulates the basic system objects to achieve discrete
 * event execution.
 */
public class Entity {
    private static final Logger LOG = LogManager.getFormatterLogger(Entity.class.getSimpleName());

    //public static final int FLAG_TRACE = 0x01; // reserved in case we want to treat tracing like the other flags
    //public static final int FLAG_TRACEREQUIRED = 0x02;
    //public static final int FLAG_TRACESTATE = 0x04;
    public static final int FLAG_LOCKED = 0x08;
    //public static final int FLAG_TRACKEVENTS = 0x10;
    public static final int FLAG_ADDED = 0x20;
    public static final int FLAG_EDITED = 0x40;
    public static final int FLAG_GENERATED = 0x80;
    public static final int FLAG_DEAD = 0x0100;

    @Keyword(description = "The list of user defined attributes for this entity.\n" +
            " The attribute name is followed by its initial value. The unit provided for" +
            "this value will determine the attribute's unit type.",
            exampleList = {"{ A 20.0 s } { alpha 42 }"})
    public final AttributeDefinitionListInput attributeDefinitionList;

    @Keyword(description = "A free form string describing the Entity", exampleList = {"'A very useful entity'"})
    protected final StringInput description;

    private long entityNumber;

    public void setEntityNumber(long entityNumber) {
        this.entityNumber = entityNumber;
    }

    private final ArrayList<Input<?>> inpList = new ArrayList<>();

    private final HashMap<String, AttributeHandle> attributeMap = new HashMap<>();

    private final BooleanInput trace;
    protected boolean traceFlag = false;
    private String entityName;
    private int flags;

    @Autowired
    protected ConfigurationManager configurationManager;

    public ConfigurationManager getConfigurationManager() {
        return configurationManager;
    }

    public void setConfigurationManager(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }

    {
        trace = new BooleanInput("Trace", "Key Inputs", false);
        trace.setHidden(true);
        this.addInput(trace);

        description = new StringInput("Description", "Key Inputs", "");
        this.addInput(description);

        attributeDefinitionList = new AttributeDefinitionListInput(this, "AttributeDefinitionList",
                "Key Inputs", new ArrayList<AttributeHandle>());
        attributeDefinitionList.setHidden(true);
        this.addInput(attributeDefinitionList);
    }

    /**
     * Constructor for entity initializing members.
     */
    public Entity() {
        flags = 0;
    }

    private static boolean isValidName(String key) {
        for (int i = 0; i < key.length(); ++i) {
            final char c = key.charAt(i);
            if (c == ' ' || c == '\t' || c == '{' || c == '}')
                return false;
        }
        return true;
    }

    long calculateDelayLength(double waitLength) {
        return Math.round(waitLength * getConfigurationManager().getSimulation().getSimTimeFactor());
    }

    public void validate() throws InputErrorException {
        for (Input<?> in : inpList) {
            in.validate();
        }
    }

    /**
     * Initialises the entity prior to the start of the model run.
     * <p/>
     * This method must not depend on any other entities so that it can be
     * called for each entity in any sequence.
     */
    public void earlyInit() {

        // Reset the attributes to their initial values
        for (AttributeHandle h : attributeMap.values()) {
            h.setValue(h.getInitialValue());
        }
    }

    /**
     * Initialises the entity prior to the start of the model run.
     * <p/>
     * This method assumes other entities have already called earlyInit.
     */
    public void lateInit() {
    }

    /**
     * Starts the execution of the model run for this entity.
     * <p/>
     * If required, initialisation that depends on another entity can be
     * performed in this method. It is called after earlyInit().
     */
    public void startUp() {
    }

    /**
     * Resets the statistics collected by the entity.
     */
    public void clearStatistics() {
    }

    /**
     * Assigns input values that are helpful when the entity is dragged and
     * dropped into a model.
     */
    public void setInputsForDragAndDrop() {
    }

    public void kill() {
        setFlag(FLAG_DEAD);
    }

    /**
     * Performs any actions that are required at the end of the simulation run, e.g. to create an output report.
     */
    public void doEnd() {
    }

    /**
     * Get the current Simulation ticks value.
     *
     * @return the current simulation tick
     */
    public final long getSimTicks() {
        return getConfigurationManager().getEventManager().simTicks();
    }

    /**
     * Get the current Simulation time.
     *
     * @return the current time in seconds
     */
    public final double getSimTime() {
        return getConfigurationManager().getEventManager().simSeconds();
    }

    public final double getCurrentTime() {
        long ticks = getSimTicks();
        return ticks / getConfigurationManager().getSimulation().getSimTimeFactor();
    }

    protected void addInput(Input<?> in) {
        inpList.add(in);
    }

    protected void addSynonym(Input<?> in, String synonym) {
        inpList.add(new SynonymInput(synonym, in));
    }

    public final Input<?> getInput(String key) {
        for (int i = 0; i < inpList.size(); i++) {
            Input<?> in = inpList.get(i);
            if (key.equals(in.getKeyword())) {
                if (in.isSynonym())
                    return ((SynonymInput) in).input;
                else
                    return in;
            }
        }

        return null;
    }

    /**
     * Copy the inputs for each keyword to the caller.  Any inputs that have already
     * been set for the caller are overwritten by those for the entity being copied.
     *
     * @param ent = entity whose inputs are to be copied
     */
    public void copyInputs(Entity ent) {
        ArrayList<String> tmp = new ArrayList<>();
        for (Input<?> sourceInput : ent.inpList) {
            if (sourceInput.isDefault() || sourceInput.isSynonym()) {
                continue;
            }
            tmp.clear();
            sourceInput.getValueTokens(tmp);
            KeywordIndex kw = new KeywordIndex(sourceInput.getKeyword(), tmp, null);
            configurationManager.apply(this, kw);
        }
    }

    public void setFlag(int flag) {
        flags |= flag;
    }

    public void clearFlag(int flag) {
        flags &= ~flag;
    }

    public boolean testFlag(int flag) {
        return (flags & flag) != 0;
    }

    public void setTraceFlag() {
        traceFlag = true;
    }

    public void clearTraceFlag() {
        traceFlag = false;
    }

    /**
     * Method to return the name of the entity.
     * Note that the name of the entity may not be the unique identifier used in the namedEntityHashMap; see Entity.toString()
     */
    public final String getName() {
        return entityName;
    }

    /**
     * Method to set the input name of the entity.
     */
    public void setName(String name) {
        if (!isValidName(name))
            throw new ErrorException("Entity names cannot contain spaces, tabs, or braces ({}).");

        if (testFlag(FLAG_GENERATED)) {
            entityName = name;
            return;
        }

        // TODO check for external renaming
    }

    /**
     * Get the unique number for this entity
     *
     * @return
     */
    public long getEntityNumber() {
        return entityNumber;
    }

    /**
     * Method to return the unique identifier of the entity. Used when building Edit tree labels
     *
     * @return entityName
     */
    @Override
    public String toString() {
        return getName();
    }

    /**
     * This method updates the Entity for changes in the given input
     */
    public void updateForInput(Input<?> input) {

        if (input == trace) {
            if (trace.getValue())
                this.setTraceFlag();
            else
                this.clearTraceFlag();

            return;
        }

        if (input == attributeDefinitionList) {
            attributeMap.clear();
            for (AttributeHandle h : attributeDefinitionList.getValue()) {
                this.addAttribute(h.getName(), h);
            }

            // Update the OutputBox
            // TODO FrameBox.reSelectEntity();
            return;
        }
    }

    public double calculateDiscreteTime(double time) {
        long discTime = calculateDelayLength(time);
        return discTime / getConfigurationManager().getSimulation().getSimTimeFactor();
    }

//    public double calculateEventTime(double waitLength) {
//        long eventTime = getSimTicks() + calculateDelayLength(waitLength);
//
//        if (eventTime < 0) {
//            eventTime = Long.MAX_VALUE;
//        }
//
//        return eventTime / Simulation.getSimTimeFactor();
//    }

//    public double calculateEventTimeBefore(double waitLength) {
//        long eventTime = getSimTicks() + (long) Math.floor(waitLength * Simulation.getSimTimeFactor());
//
//        if (eventTime < 0) {
//            eventTime = Long.MAX_VALUE;
//        }
//
//        return eventTime / Simulation.getSimTimeFactor();
//    }

//    public double calculateEventTimeAfter(double waitLength) {
//        long eventTime = getSimTicks() + (long) Math.ceil(waitLength * Simulation.getSimTimeFactor());
//        return eventTime / Simulation.getSimTimeFactor();
//    }

    public final void startProcess(String methodName, Object... args) {
        ProcessTarget t = new ReflectionTarget(this, methodName, args);
        startProcess(t);
    }

    public final void startProcess(ProcessTarget t) {
        getConfigurationManager().getEventManager().startProcess(t);
    }

    public final void scheduleProcess(double secs, int priority, ProcessTarget t) {
        getConfigurationManager().getEventManager().scheduleSeconds(secs, priority, false, t, null);
    }

    public final void scheduleProcess(double secs, int priority, ProcessTarget t, EventHandle handle) {
        getConfigurationManager().getEventManager().scheduleSeconds(secs, priority, false, t, handle);
    }

    public final void scheduleProcess(double secs, int priority, boolean fifo, ProcessTarget t, EventHandle handle) {
        getConfigurationManager().getEventManager().scheduleSeconds(secs, priority, fifo, t, handle);
    }

    public final void scheduleProcessTicks(long ticks, int priority, boolean fifo, ProcessTarget t, EventHandle h) {
        getConfigurationManager().getEventManager().scheduleTicks(ticks, priority, fifo, t, h);
    }

    public final void scheduleProcessTicks(long ticks, int priority, ProcessTarget t) {
        getConfigurationManager().getEventManager().scheduleTicks(ticks, priority, false, t, null);
    }

    public final void waitUntil(Conditional cond, EventHandle handle) {
        // Don't actually wait if the condition is already true
        if (cond.evaluate()) return;
        getConfigurationManager().getEventManager().waitUntil(cond, handle);
    }

    /**
     * Wait a number of simulated seconds and a given priority.
     *
     * @param secs
     * @param priority
     */
    public final void simWait(double secs, int priority) {
        getConfigurationManager().getEventManager().waitSeconds(secs, priority, false, null);
    }

    /**
     * Wait a number of simulated seconds and a given priority.
     *
     * @param secs
     * @param priority
     */
    public final void simWait(double secs, int priority, EventHandle handle) {
        getConfigurationManager().getEventManager().waitSeconds(secs, priority, false, handle);
    }

    /**
     * Wait a number of simulated seconds and a given priority.
     *
     * @param secs
     * @param priority
     */
    public final void simWait(double secs, int priority, boolean fifo, EventHandle handle) {
        getConfigurationManager().getEventManager().waitSeconds(secs, priority, fifo, handle);
    }

    /**
     * Wait a number of discrete simulation ticks and a given priority.
     *
     * @param ticks
     * @param priority
     */
//    public final void simWaitTicks(long ticks, int priority) {
//        EventManager.waitTicks(ticks, priority, false, null);
//    }

    /**
     * Wait a number of discrete simulation ticks and a given priority.
     *
     * @param ticks
     * @param priority
     * @param fifo
     * @param handle
     */
//    public final void simWaitTicks(long ticks, int priority, boolean fifo, EventHandle handle) {
//        EventManager.waitTicks(ticks, priority, fifo, handle);
//    }

    /**
     * Wrapper of eventManager.scheduleWait(). Used as a syntax nicity for
     * calling the wait method.
     *
     * @param duration The duration to wait
     * @param priority The relative priority of the event scheduled
     */
//    public final void scheduleWait(double duration, int priority) {
//        long waitLength = calculateDelayLength(duration);
//        if (waitLength == 0)
//            return;
//        EventManager.waitTicks(waitLength, priority, false, null);
//    }

    /**
     * Wrapper of eventManager.scheduleWait(). Used as a syntax nicity for
     * calling the wait method.
     *
//     * @param duration The duration to wait
//     * @param priority The relative priority of the event scheduled
     */
//    public final void scheduleWait(double duration, int priority, EventHandle handle) {
//        long waitLength = calculateDelayLength(duration);
//        if (waitLength == 0)
//            return;
//        EventManager.waitTicks(waitLength, priority, false, handle);
//    }

    public void handleSelectionLost() {
    }

    // ******************************************************************************************************
    // EDIT TABLE METHODS
    // ******************************************************************************************************

    public ArrayList<Input<?>> getEditableInputs() {
        return inpList;
    }

    public void error(String fmt, Object... args) throws ErrorException {
        final StringBuilder sb = new StringBuilder(this.getName());
        sb.append(": ");
        sb.append(String.format(fmt, args));
        throw new ErrorException(sb.toString());
    }

    /**
     * Print an error message.
     */
    public void error(String meth, String text1, String text2) {
        double time = 0.0d;
        if (getConfigurationManager().getEventManager().hasCurrent())
            time = getCurrentTime();
        LOG.error("Time:%.5f Entity:%s%n%s%n%s%n%s%n", time, getName(), meth, text1, text2);

        // We don't want the model to keep executing, throw an exception and let
        // the higher layers figure out if we should terminate the run or not.
        throw new ErrorException("ERROR: %s", getName());
    }

    /**
     * Print a warning message.
     */
    public void warning(String meth, String text1, String text2) {
        double time = 0.0d;
        if (getConfigurationManager().getEventManager().hasCurrent()) {
            time = getCurrentTime();
        }
        LOG.warn("Time:%.5f Entity:%s%n%s%n%s%n%s%n",
                time, getName(),
                meth, text1, text2);
    }

    /**
     * Returns a user specific unit type. This is needed for entity types like distributions that may change the unit type
     * that is returned at runtime.
     *
     * @return
     */
    public Class<? extends Unit> getUserUnitType() {
        return DimensionlessUnit.class;
    }


    public final OutputHandle getOutputHandle(String outputName) {
        if (hasAttribute(outputName))
            return attributeMap.get(outputName);

        if (hasOutput(outputName)) {
            OutputHandle ret = new OutputHandle(this, outputName);
            if (ret.getUnitType() == UserSpecifiedUnit.class)
                ret.setUnitType(getUserUnitType());

            return ret;
        }

        return null;
    }

    /**
     * Optimized version of getOutputHandle() for output names that are known to be interned
     *
     * @param outputName
     * @return
     */
    public final OutputHandle getOutputHandleInterned(String outputName) {
        if (hasAttribute(outputName))
            return attributeMap.get(outputName);

        if (OutputHandle.hasOutputInterned(this.getClass(), outputName)) {
            OutputHandle ret = new OutputHandle(this, outputName);
            if (ret.getUnitType() == UserSpecifiedUnit.class)
                ret.setUnitType(getUserUnitType());

            return ret;
        }

        return null;
    }

    public boolean hasOutput(String outputName) {
        if (OutputHandle.hasOutput(this.getClass(), outputName))
            return true;
        if (attributeMap.containsKey(outputName))
            return true;

        return false;
    }

    /**
     * Writes the entry in the output report for this entity.
     *
     * @param file    - the file in which the outputs are written
     * @param simTime - simulation time at which the outputs are evaluated
     */
    public void printReport(FileEntity file, double simTime) {

        // Loop through the outputs
        boolean linePrinted = false;
        ArrayList<OutputHandle> handles = OutputHandle.getOutputHandleList(this);
        for (OutputHandle o : handles) {

            // Should this output appear in the report?
            if (!o.isReportable())
                continue;

            // Is there a preferred unit in which to display the output?
            Class<? extends Unit> ut = o.getUnitType();
            String unitString = Unit.getDisplayedUnit(ut);
            double factor = Unit.getDisplayedUnitFactor(ut);

            // Is the output a number?
            String s;
            if (o.isNumericValue())
                s = String.valueOf(o.getValueAsDouble(simTime, Double.NaN) / factor);
            else {
                unitString = Unit.getSIUnit(ut);  // lists of doubles are not converted to preferred units yet
                s = o.getValue(simTime, o.getReturnType()).toString();
            }

            // Does the output require a unit to be shown?
            linePrinted = true;
            if (ut == Unit.class || ut == DimensionlessUnit.class) {
                file.format("%s\tOutput[%s]\t%s%n",
                        this.getName(), o.getName(), s);
            } else {
                file.format("%s\tOutput[%s, %s]\t%s%n",
                        this.getName(), o.getName(), unitString, s);
            }
        }
        if (linePrinted)
            file.format("%n");
    }

    public void printReport(ByteArrayOutputStream outputStream, double simTime) throws IOException {

        // Loop through the outputs
        boolean linePrinted = false;
        ArrayList<OutputHandle> handles = OutputHandle.getOutputHandleList(this);
        for (OutputHandle o : handles) {

            // Should this output appear in the report?
            if (!o.isReportable())
                continue;

            // Is there a preferred unit in which to display the output?
            Class<? extends Unit> ut = o.getUnitType();
            String unitString = Unit.getDisplayedUnit(ut);
            double factor = Unit.getDisplayedUnitFactor(ut);

            // Is the output a number?
            String s;
            if (o.isNumericValue())
                s = String.valueOf(o.getValueAsDouble(simTime, Double.NaN) / factor);
            else {
                unitString = Unit.getSIUnit(ut);  // lists of doubles are not converted to preferred units yet
                s = o.getValue(simTime, o.getReturnType()).toString();
            }

            // Does the output require a unit to be shown?
            linePrinted = true;
            if (ut == Unit.class || ut == DimensionlessUnit.class) {
                outputStream.write(String.format("%s\tOutput[%s]\t%s%n",
                        this.getName(), o.getName(), s).getBytes());
            } else {
                outputStream.write(String.format("%s\tOutput[%s, %s]\t%s%n",
                        this.getName(), o.getName(), unitString, s).getBytes());
            }
        }
        if (linePrinted)
            outputStream.write(String.format("%n").getBytes());
    }

    @Output(name = "Name", description = "The unique input name for this entity.")
    public String getNameOutput(double simTime) {
        return entityName;
    }

    @Output(name = "Description", description = "A string describing this entity.")
    public String getDescription(double simTime) {
        return description.getValue();
    }

    @Output(name = "SimTime",
            description = "The present simulation time.",
            unitType = TimeUnit.class)
    public double getSimTime(double simTime) {
        return simTime;
    }

    private void addAttribute(String name, AttributeHandle h) {
        attributeMap.put(name, h);
    }

    public boolean hasAttribute(String name) {
        return attributeMap.containsKey(name);
    }

    public Class<? extends Unit> getAttributeUnitType(String name) {
        AttributeHandle attributeHandle = attributeMap.get(name);
        if (attributeHandle == null)
            return null;
        return attributeHandle.getUnitType();
    }

    public void setAttribute(String name, double value, Class<? extends Unit> unit) {
        AttributeHandle attributeHandle = attributeMap.get(name);
        if (attributeHandle == null)
            this.error("Invalid attribute name: %s", name);

        if (attributeHandle.getUnitType() != unit) {
            this.error("Invalid unit returned by an expression. Received: %s, expected: %s",
                    unit.getSimpleName(), attributeHandle.getUnitType().getSimpleName(), "");
        }

        attributeHandle.setValue(value);
    }

    public ArrayList<String> getAttributeNames() {
        ArrayList<String> attributeNames = new ArrayList<>();
        for (String name : attributeMap.keySet()) {
            attributeNames.add(name);
        }
        return attributeNames;
    }

    public ObjectType getObjectType() {
        return ObjectType.getObjectTypeForClass(this.getClass());
    }

    @Output(name = "ObjectType", description = "The class of objects that this entity belongs to.")
    public String getObjectTypeName(double simTime) {
        return this.getObjectType().getName();
    }
}
