/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2014 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.basicsim;

import com.jaamsim.events.ProcessTarget;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class EndModelTarget extends ProcessTarget {
	private static final Logger LOG = LogManager.getFormatterLogger(EndModelTarget.class.getSimpleName());

    public EndModelTarget() {
        LOG.info("Added print report target process");
    }

    @Override
    public String getDescription() {
        return "SimulationEnd";
    }

    @Override
    public void process() {
        LOG.info("Processing print report target process ...");
        getConfigurationManager().getEventManager().pause();
        for (int i = 0; i < getConfigurationManager().getEntityManager().getAll().size(); i++) {
            getConfigurationManager().getEntityManager().getAll().get(i).doEnd();
        }

        // Print the output report
        LOG.info("Should print report: " + getConfigurationManager().getSimulation().getPrintReport());
        if (getConfigurationManager().getSimulation().getPrintReport())
            getConfigurationManager().getEventManager().printReport(getConfigurationManager().getSimulation().getEndTime());

        LOG.info("Made it to do end at");
        getConfigurationManager().shutdown();

    }
}
