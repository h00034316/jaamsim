package com.jaamsim.probability;

import com.jaamsim.ProbabilityDistributions.BetaDistribution;
import com.jaamsim.basicsim.Entity;
import com.jaamsim.basicsim.EntityManager;
import com.jaamsim.basicsim.ObjectType;
import com.jaamsim.basicsim.Simulation;
import com.jaamsim.input.ConfigurationManager;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestBetaDistribution {

    @Test
    public void MeanAndStandardDeviation() {
        EntityManager entityManager = new EntityManager(new Simulation());
        ConfigurationManager configurationManager = new ConfigurationManager(entityManager);
        ObjectType t = entityManager.defineEntityWithUniqueName(ObjectType.class, "TestType", "-", true);
        configurationManager.applyArgs(t, "JavaClass", "com.jaamsim.units.DimensionlessUnit");

        BetaDistribution dist = entityManager.defineEntityWithUniqueName(BetaDistribution.class, "Dist2", "-", true);
        dist.setConfigurationManager(configurationManager);
        configurationManager.applyArgs(dist, "UnitType", t.getName());
        configurationManager.applyArgs(dist, "AlphaParam", "2.0");
        configurationManager.applyArgs(dist, "BetaParam", "2.0");
        configurationManager.applyArgs(dist, "RandomSeed", "0");
        dist.validate();
        dist.earlyInit();

        int numSamples = 100000;
        double total = TestContinuousDistribution.sampleDistribution(dist, numSamples);
        double mean = total / numSamples;

        assertTrue(Math.abs(dist.getSampleMean(0.0) - mean) < 0.001);
        assertTrue(Math.abs(dist.getSampleMean(0.0) / dist.getMeanValue(0.0) - 1.0) < 0.001);
        assertTrue(Math.abs(dist.getSampleStandardDeviation(0.0) / dist.getStandardDeviation(0.0) - 1.0) < 0.005);
    }


}
