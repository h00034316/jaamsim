/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2013 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.probability;

import com.jaamsim.ProbabilityDistributions.ContinuousDistribution;
import com.jaamsim.Samples.SampleProvider;
import com.jaamsim.basicsim.Entity;
import com.jaamsim.basicsim.EntityManager;
import com.jaamsim.basicsim.ObjectType;
import com.jaamsim.basicsim.Simulation;
import com.jaamsim.events.EventManager;
import com.jaamsim.events.ProcessTarget;
import com.jaamsim.events.TestFrameworkHelpers;
import com.jaamsim.input.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestContinuousDistribution {
    private static final Logger LOG = LogManager.getFormatterLogger(TestContinuousDistribution.class.getSimpleName());

    static double sampleDistribution(SampleProvider distribution, int numSamples) {
        SampleDistribution processTarget = new SampleDistribution(distribution, numSamples);
        EventManager eventManager = new EventManager("DistibutionUnitTest");
        EntityManager entityManager = new EntityManager();
        ConfigurationManager configurationManager = new ConfigurationManager(entityManager);
        configurationManager.setEventManager(eventManager);

        // TODO entities should be passed the event manager
        ((Entity) distribution).setConfigurationManager(configurationManager);
        eventManager.clear();

        eventManager.scheduleProcessExternal(0, 0, false, processTarget, null);
        TestFrameworkHelpers.runEventsToTick(eventManager, Long.MAX_VALUE, 100000000);
        return processTarget.total;
    }

    @Test
    public void MeanAndStandardDeviation() {
        EntityManager entityManager = new EntityManager(new Simulation());
        ConfigurationManager configurationManager = new ConfigurationManager(entityManager);
        configurationManager.setEntityManager(entityManager);
        ObjectType testType = entityManager.defineEntityWithUniqueName(ObjectType.class, "TestType", "-", true);
        configurationManager.applyArgs(testType, "JavaClass", "com.jaamsim.units.DimensionlessUnit");

        ContinuousDistribution distribution = entityManager.defineEntityWithUniqueName(ContinuousDistribution.class, "Dist", "-", true);
        distribution.setConfigurationManager(configurationManager);
        configurationManager.applyArgs(distribution, "UnitType", testType.getName());
        configurationManager.applyArgs(distribution, "ValueList", "1.0", "3.0", "5.0", "10.0");
        configurationManager.applyArgs(distribution, "CumulativeProbabilityList", "0.0", "0.5", "0.8", "1.0");
        configurationManager.applyArgs(distribution, "RandomSeed", "1");
        distribution.validate();
        distribution.earlyInit();

        int numSamples = 1000000;
        double total = TestContinuousDistribution.sampleDistribution(distribution, numSamples);
        double mean = total / numSamples;

        assertTrue(Math.abs(distribution.getSampleMean(0.0) - mean) < 0.001);
        assertTrue(Math.abs(distribution.getSampleMean(0.0) / distribution.getMeanValue(0.0) - 1.0) < 0.001);
        assertTrue(Math.abs(distribution.getSampleStandardDeviation(0.0) / distribution.getStandardDeviation(0.0) - 1.0) < 0.001);
    }

    static class SampleDistribution extends ProcessTarget {
        final SampleProvider distribution;
        final int numSamples;
        double total;

        public SampleDistribution(SampleProvider distribution, int numSamples) {
            this.distribution = distribution;
            this.numSamples = numSamples;
        }

        @Override
        public String getDescription() {
            return "DistibutionUnitTest";
        }

        @Override
        public void process() {
            total = 0.0d;
            for (int i = 0; i < numSamples; i++) {
                total += distribution.getNextSample(0.0d);
            }
        }
    }
}
