/*
 * JaamSim Discrete Event Simulation
 * Copyright (C) 2013 Ausenco Engineering Canada Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package com.jaamsim.probability;

import com.jaamsim.ProbabilityDistributions.WeibullDistribution;
import com.jaamsim.basicsim.Entity;
import com.jaamsim.basicsim.EntityManager;
import com.jaamsim.basicsim.ObjectType;
import com.jaamsim.basicsim.Simulation;
import com.jaamsim.input.ConfigurationManager;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestWeibullDistribution {

    @Test
    public void MeanAndStandardDeviation() {
        EntityManager entityManager = new EntityManager(new Simulation());
        ConfigurationManager configurationManager = new ConfigurationManager(entityManager);
        ObjectType t = entityManager.defineEntityWithUniqueName(ObjectType.class, "TestType", "-", true);
        configurationManager.applyArgs(t, "JavaClass", "com.jaamsim.units.DimensionlessUnit");

        WeibullDistribution dist = entityManager.defineEntityWithUniqueName(WeibullDistribution.class, "Dist", "-", true);
        dist.setConfigurationManager(configurationManager);
        configurationManager.applyArgs(dist, "UnitType", t.getName());
        configurationManager.applyArgs(dist, "Scale", "10.0");
        configurationManager.applyArgs(dist, "Shape", "2.0");
        configurationManager.applyArgs(dist, "RandomSeed", "0");
        dist.validate();
        dist.earlyInit();

        int numSamples = 1000000;
        double total = TestContinuousDistribution.sampleDistribution(dist, numSamples);
        double mean = total / numSamples;

        assertTrue(Math.abs(dist.getSampleMean(0.0) - mean) < 0.001);
        assertTrue(Math.abs(dist.getSampleMean(0.0) / dist.getMeanValue(0.0) - 1.0) < 0.001);
        assertTrue(Math.abs(dist.getSampleStandardDeviation(0.0) / dist.getStandardDeviation(0.0) - 1.0) < 0.001);

        WeibullDistribution dist2 = entityManager.defineEntityWithUniqueName(WeibullDistribution.class, "Dist", "-", true);
        dist2.setConfigurationManager(configurationManager);
        configurationManager.applyArgs(dist2, "UnitType", t.getName());
        configurationManager.applyArgs(dist2, "Scale", "10.0");
        configurationManager.applyArgs(dist2, "Shape", "2.0");
        configurationManager.applyArgs(dist2, "Location", "100.0");
        configurationManager.applyArgs(dist2, "RandomSeed", "0");
        dist2.validate();
        dist2.earlyInit();

        double total2 = TestContinuousDistribution.sampleDistribution(dist2, numSamples);
        double mean2 = total2 / numSamples;
        assertTrue(Math.abs(dist2.getSampleMean(0.0) - mean2) < 0.001);
        assertTrue(Math.abs(dist2.getSampleMean(0.0) / dist2.getMeanValue(0.0) - 1.0) < 0.001);
        assertTrue(Math.abs(dist2.getSampleStandardDeviation(0.0) / dist2.getStandardDeviation(0.0) - 1.0) < 0.001);

        assertTrue(Math.abs(dist.getSampleStandardDeviation(0.0) - dist2.getSampleStandardDeviation(0.0)) < 0.000001);
        assertTrue(Math.abs(dist.getStandardDeviation(0.0) - dist2.getStandardDeviation(0.0)) < 0.000001);
        assertTrue(Math.abs(dist.getSampleMean(0.0) - dist2.getSampleMean(0.0) + 100.0) < 0.000001);
    }
}
